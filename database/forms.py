from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from .hold_global_vars import *
from .models import *


class ModelMultipleChoiceWithEmptyField(forms.ModelMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        super(ModelMultipleChoiceWithEmptyField, self).__init__(*args, **kwargs)
        self.choices = [("", "")] + list(self.choices)

    def clean(self, value):
        if self.required and not value:
            raise ValidationError(self.error_messages['required'], code='required')
        if value == [u'0']:
            return value
        return super(ModelMultipleChoiceWithEmptyField,self).clean(value)


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password', 'confirm_password', 'email','first_name','last_name']


class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']

class SamplesSearchForm(forms.Form):
    name = forms.CharField(max_length=100, required=False)
    # species = forms.ChoiceField(choices=SPECIE_CHOICES, required=False) # Whenever new specie would be added
    tissue = forms.ChoiceField(choices=[], required=False)
    # cell_type = forms.ChoiceField(choices=[], required=False) #Only after T cells genotypes would be added
    sub_cell_type = forms.ChoiceField(choices=[], required=False, label= "Cell subset type")
    isotype = forms.ChoiceField(choices=[], required=False)
    helix = forms.ChoiceField(choices=HELIX_CHOICES, required=False, label ="Target substrate")
    sequencing_length = forms.ChoiceField(choices=[], required=False)

    def __init__(self, *args, **kwargs):
        super(SamplesSearchForm, self).__init__(*args, **kwargs)
        self.fields['sequencing_length'].choices = [("","")] + [(x[0], x[0]) for x in Seq_Protocols.objects.all().values_list("sequencing_length").distinct()]
        self.fields['tissue'].choices= [("","")] + [(x[0], x[0]) for x in Tissue_Pro.objects.all().values_list("tissue").distinct()]
        self.fields['sub_cell_type'].choices= [("","")] + [(x[0], x[0]) for x in Tissue_Pro.objects.all().values_list("sub_cell_type").distinct()]
        self.fields['isotype'].choices= [("","")] + [(x[0], x[0]) for x in Tissue_Pro.objects.all().values_list("isotype").distinct()]
        # Only after T cells genotypes would be added
        # self.fields['cell_type'].choices= [("","")] + [(x[0], x[0]) for x in Tissue_Pro.objects.all().values_list("cell_type").distinct()]


class SamplesAdvanceSearchForm(forms.Form):
    chain = forms.ChoiceField(choices=[], required=False, label="Locus")
    sex = forms.ChoiceField(choices=SEX_CHOICES, required=False)
    patient = forms.ModelChoiceField(Patients.objects.all().order_by('name'), empty_label="", required=False, label="Subject name (VDJbase format)")
    name_in_paper = forms.ChoiceField(choices=[], required=False, label="Subject ID (as assigned by submitter)")
    health_status = forms.CharField(max_length=50, required=False, label="Diagnosis")
    umi = forms.NullBooleanField(required=False)
    reads_number_gte = forms.IntegerField(min_value=0, required=False, initial=0, label="Minimal no. of usable reads")
    sequencing_platform = forms.ChoiceField(choices=[], required=False)
    primer_3_prime = forms.ChoiceField(choices=[], required=False, label= "Reverse PCR primer target")
    primer_5_prime = forms.ChoiceField(choices=[], required=False, label="Forward PCR primer target")
    haplotype_by = forms.ChoiceField(choices=[], required=False)
    rows = forms.IntegerField(min_value=20, max_value=200, label="No. table rows in page:", required=False)


    def __init__(self, *args, **kwargs):
        super(SamplesAdvanceSearchForm, self).__init__(*args, **kwargs)
        self.fields['name_in_paper'].choices = [("","")] + [(x[0], x[0]) for x in Patients.objects.all().order_by('name_in_paper').values_list('name_in_paper').distinct()]
        self.fields['sequencing_platform'].choices = [("","")] + [(x[0], x[0]) for x in Seq_Protocols.objects.all().values_list("sequencing_platform").distinct()]
        self.fields['primer_3_prime'].choices = [("","")] + [(x[0], x[0]) for x in Seq_Protocols.objects.all().values_list("primers_3_location").distinct()]
        self.fields['primer_5_prime'].choices = [("","")] + [(x[0], x[0]) for x in Seq_Protocols.objects.all().values_list("primers_5_location").distinct()]
        self.fields['haplotype_by'].choices = [("","")] + [(x[0], x[0]) for x in Haplotypes_files.objects.all().values_list("by_gene").distinct()]
        self.fields['chain'].choices= [("","")] + [(x[0], x[0]) for x in Samples.objects.all().values_list("chain").distinct()]



class SampleFilterByAlleles(forms.Form):
    study = ModelMultipleChoiceWithEmptyField(Studies.objects.all().order_by('name'), required=False, label="Studies",
                                              widget=forms.SelectMultiple(
                                                  attrs={'size': '8', 'style': 'width:100%; max-width:100px;',
                                                         'data-toggle': "tooltip",
                                                         'data-placement': "bottom",
                                                         'title': "VDJbase study name format. Use Ctrl to make multiple selections."}))
    alleles_s = ModelMultipleChoiceWithEmptyField(Alleles.objects.none(), required=False, label="Alleles",
            widget=forms.SelectMultiple(attrs={'size': '8', 'style': 'width:100%; max-width:300px;', 'data-toggle': "tooltip", 'data-placement': "bottom", 'title': "Use Ctrl to make multiple selections"}))

    def __init__(self, *args, **kwargs):
        super(SampleFilterByAlleles, self).__init__(*args, **kwargs)
        alleles = list(Alleles.objects.all().order_by(SORT_ALLELES_BY_SEGMENT, 'name'))
        alleles = [(x.id, x.name) for x in alleles]
        self.fields['alleles_s'].choices = alleles
        self.fields['alleles_s'].widget.attrs.update({'class': 'filter_gene'})


class SampleFilterByGene(forms.Form):
    genes_type = forms.MultipleChoiceField(choices=[], required=False, label="Gene types",
        widget=forms.SelectMultiple(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Use Ctrl to make multiple selections"}))
    kdiff = forms.DecimalField(min_value=0, max_digits=6, decimal_places=1, initial=0, required=False)
    with_pseudo_genes = forms.ChoiceField(choices=WITH_PSEUDO_CHOICES, widget=forms.RadioSelect, required=False, initial=0)
    genes = ModelMultipleChoiceWithEmptyField(Gene.objects.none(), required=False,
            widget=forms.SelectMultiple(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Use Ctrl to make multiple selections"}))

    def __init__(self, *args, **kwargs):
        super(SampleFilterByGene, self).__init__(*args, **kwargs)
        self.fields['genes'].queryset = Gene.objects.all().order_by(SORT_GENES_BY_SEGMENT, 'name')
        self.fields['genes_type'].choices = [("", "")] + [(x, x) for x in SEGMENTS_ORDER]
        self.fields['genes_type'].widget.attrs.update({'class': 'filter_gene'})
        self.fields['genes'].widget.attrs.update({'class': 'filter_gene'})
        self.fields['kdiff'].widget.attrs.update({'class': 'filter_gene'})

class AllelesSearchForm(forms.Form):
    name = forms.CharField(max_length=100, required=False)
    sample = forms.CharField(max_length=100, required=False)
    similar = forms.CharField(max_length=100, required=False, label="Identical sequence")
    show_imgt = forms.BooleanField(initial=True, label="Show IMGT", required=False,
        widget=forms.CheckboxInput(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Show alleles defined in the IMGT reference set"}))
    show_ambiguous = forms.BooleanField(initial=True, label="Show Ambiguous", required=False,
        widget=forms.CheckboxInput(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Show calls that match more than one allele in the IMGT reference set, either because of sequence truncation or presence of n-nucleotides"}))
    show_novel = forms.BooleanField(initial=True, label="Show Novel", required=False,
        widget=forms.CheckboxInput(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Show novel alleles inferred by TIgGER"}))
    show_deletions = forms.BooleanField(initial=True, label="Show Deletions", required=False,
        widget=forms.CheckboxInput(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Show gene deletions identified by RAbHIT"}))
    show_low_confidence = forms.BooleanField(initial=False, label="Show Low Confidence", required=False,
        widget=forms.CheckboxInput(attrs={'size': '8', 'data-toggle': "tooltip", 'data-placement': "right", 'title': "Show inferences that have warnings often associated with sequencing-related issues"}))

    def __init(self, *args, **kwargs):
        super(AllelesSearchForm, self).__init__(*args, **kwargs)
        self.fields['gene'].queryset = Gene.objects.all().order_by(SORT_GENES_BY_SEGMENT, 'name')


class HaplotypeGraphForm(forms.Form):
    graph_by_gene = forms.ChoiceField(choices=[], required=False, label="Haploype by")
    haplo_k = forms.DecimalField(min_value=0, max_digits=6, decimal_places=1, initial=0, required=False, label="K minimal threshold")

    def __init__(self, *args, **kwargs):
        super(HaplotypeGraphForm, self).__init__(*args, **kwargs)
        self.fields['graph_by_gene'].choices = [(x[0], x[0]) for x in Haplotypes_files.objects.all()
            .order_by("-by_gene").values_list("by_gene").distinct()]

