# Each installation requires a file local_settings.py which should at a minimum contain the settings below, suitably
# modified for your installation, and can in addition contain other definitions to over-ride those in settings.py.
# local_settings.py should not be published: it is specific to the machine and contains information that should be kept private


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*(np25b$c4v2vz=8ekl8j@@4lp!hh6o&*7do5jpj3z=5qeuj@('

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '132.70.224.29', 'vdjbase.eng.biu.ac.il', 'www.vdjbase.org']

