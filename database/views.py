from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import View
from django.contrib import messages
from django.db.models import Q, Count
from urllib import parse

from .forms import *
from scripts import graphs, export_data, exportR_data, export_genotypes
from website.settings import SCRATCH_ROOT, BASE_DIR, APACHE_SEND
from .hold_global_vars import *

from itertools import chain
from wsgiref.util import FileWrapper
from collections import defaultdict

from datetime import datetime
import zipfile, os, math, datetime
import json

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna

from io import StringIO

from .apps import commit_id
from .novel_checks import check_novel_confidence

##########################################################################################################
#
#     ********************* SEND FILE ********************************
#
#     Send file via apache mod_xsendfile, or stream if we don't have it
#
##########################################################################################################

def send_file(filename, content_type, disposition):
    if APACHE_SEND:
        resp = HttpResponse()
        resp['X-Sendfile'] = filename
    else:
        # check that the file is in a configured location
        if os.path.join(BASE_DIR, 'uploads') not in filename and SCRATCH_ROOT not in filename:
            print('Only files in BASE_DIR/uploads and SCRATCH_ROOT can be sent to the user.')
            return ''
        resp = HttpResponse(open(filename, 'rb').read())

    resp['Content-Type'] = content_type
    resp['Content-Disposition'] = disposition
    return resp


##########################################################################################################
#
#     ********************* HOMEPAGE ********************************
#
##########################################################################################################

def index(request):
    template = 'homepage.html'
    all_gene = Gene.objects.all()
    context = {'all_gene': all_gene, 'commit_id': commit_id}
    return render(request, template,  context)


##########################################################################################################
#
#     ********************* SAMPLES PAGE ********************************
#
##########################################################################################################

class sample_table(View):
    template_name = 'tables/samples.html'
    form_class = SamplesSearchForm
    advance = SamplesAdvanceSearchForm
    filter_by_gene = SampleFilterByGene
    filter_samples_by_gene = SampleFilterByAlleles
    haplotype_graph_form = HaplotypeGraphForm

    ##########################################################################################################
    #
    #     ****************** EXPORT THE DATA THROUGH ZIP FILE *****************************
    #
    ##########################################################################################################

    def export_sample_table(self, name, all_samples):
        genotypes_folder = "Genotypes"
        genotypes_stats_folder = "Genotypes_stats"
        haplotypes_folder = "Haplotypes"

        titles = ["SAMPLE", "SUBJECT", "NAME_IN_PAPER", "SEX", "AGE", "ETHNICITY", "STUDY_GROUP", "DIAGNOSIS", "ORGANISM",
                  "TISSUE", "CELL_TYPE", "CELL_SUBSET_TYPE", "ISOTYPE", "TARGET_LOCUS(IG)", "TARGET_SUBSTRATE", "UMI", "SEQUENCING_PLATFORM",
                  "SEQUENCING_LENGTH", "REVERSE_PCR_PRIMER_LOCATION", "FORWARD_PCR_PRIMERS_LOCATION", "TOTAL_READS", "PROJECT",
                  "LAB_ADDRESS", "RESEARCHER", "NUMBER_OF_SUBJECTS", "NUMBER_OF_SAMPLES", "PAPER", "CONTACT",
                  "STUDY_ID", "PUBLICATION_URL", "Preprocessing", "ALIGNER_TOOL", "ALIGNER_VERSION",
                  "GERMLINE_GENE_SET", "GENOTYPING_TOOL", "GENOTYPING_VERSION", "HAPLOTYPING_TOOL", "HAPLOTYPING_VERSION",
                  "GENOTYPE_FILE", "GENOTYPE_STATISTICS", "HAPLOTYPE_BY", "HAPLOTYPE_FILES"]

        samples = []
        genotypes = []
        genotypes_stats = []
        haplotypes = []

        for s in all_samples:
            sample = []
            sample.append(s.name)

            patient = s.patient
            sample += [patient.name, patient.name_in_paper, patient.sex, patient.age, patient.ethnic, patient.cohort, patient.status]

            tissue = s.tissue_pro
            sample += [tissue.species, tissue.tissue, tissue.cell_type, tissue.sub_cell_type, tissue.isotype]
            sample.append(s.chain)

            protocol = s.seq_protocol
            sample += [protocol.helix, protocol.umi, protocol.sequencing_platform, protocol.sequencing_length,
                       protocol.primers_3_location, protocol.primers_5_location]
            sample.append(s.row_reads)

            study = s.study
            sample += [study.name, study.institute, study.researcher, study.num_subjects, study.num_samples,
                        study.reference, study.contact, study.accession_id, study.accession_reference]

            geno = s.geno_detection
            sample += [geno.prepro_tool, geno.aligner_tool, geno.aligner_ver, geno.aligner_reference, geno.geno_tool,
                       geno.geno_ver, geno.haplotype_tool, geno.haplotype_ver]

            sample.append(os.path.join(genotypes_folder, os.path.basename(s.genotype.name)))
            genotypes.append(s.genotype.name)
            if s.genotype_stats:
                sample.append(os.path.join(genotypes_stats_folder, os.path.basename(s.genotype_stats.name)))
                genotypes_stats.append(s.genotype_stats.name)
            else:
                sample.append("")

            haplotypes_by = []
            haplotypes_files = []

            if s.haplotype:
                for hap_file in s.haplotype.all():
                    haplotypes.append(hap_file.file.name)
                    haplotypes_files.append(os.path.join(haplotypes_folder, os.path.basename(hap_file.file.name)))
                    haplotypes_by.append(hap_file.by_gene)


            haplotypes_by = ", ".join(haplotypes_by)
            haplotypes_files = ", ".join(haplotypes_files)

            sample.append(haplotypes_by)
            sample.append(haplotypes_files)

            # append the list of fileds of one sample into the table f samples
            samples.append(sample)

        metafile = export_data.output("metadata.tab", titles, samples)
        name = os.path.join(SCRATCH_ROOT, name) + '.zip'

        with zipfile.ZipFile(name, 'w') as myzip:
            myzip.write(metafile, os.path.basename(metafile))
            for f in genotypes:
                path = os.path.join(BASE_DIR, 'uploads/', f)
                myzip.write(path, os.path.join(genotypes_folder, os.path.basename(path)))
            for hap in haplotypes:
                path = os.path.join(BASE_DIR, 'uploads/', hap)
                myzip.write(path, os.path.join(haplotypes_folder, os.path.basename(path)))
            for geno_stats in genotypes_stats:
                path = os.path.join(BASE_DIR, 'uploads/', geno_stats)
                myzip.write(path, os.path.join(genotypes_stats_folder, os.path.basename(path)))

            myzip.close()

        return send_file(name, 'application/zip', 'Content-Disposition')

    ##########################################################################################################
    #
    #     genotypes_graph - CREATE MULTIPLE GENOTYPES AND EXPORT
    #
    ##########################################################################################################

    def genotypes_graph(self, request, name, samples, genes, file_type, with_pseudo_genes):
        samples = samples.values_list('name', 'genotype')
        genes = genes.values_list('name')
        genes = list(chain.from_iterable(genes))
        with_pseudo_genes = bool(with_pseudo_genes)

        genotypes_graph = export_genotypes.merge_genotypes(samples, name, genes, type_html=file_type, pseudo_genes=with_pseudo_genes)

        if not genotypes_graph:
            return False

        if not file_type:
            return send_file(genotypes_graph, 'application/pdf', 'attachment; filename=genotypes_graph.pdf')
        else:
            genotypes_graph = genotypes_graph.split("graphs/")[1]
            template = "temp/display_genotype.html"
            context = {'html_file': genotypes_graph, }
            return render(request, template, context)


    ##########################################################################################################
    #
    #     genotypes_heatmap - CREATE HEATMAP GENOTYPES AND EXPORT
    #
    ##########################################################################################################

    def genotypes_heatmap(self, request, name, samples, genes, kdiff, file_type):
        samples = samples.values_list('name', 'genotype')
        genes = genes.values_list('name')
        genes = list(chain.from_iterable(genes))
        genotypes_graph = export_genotypes.genotypes_heatmap(samples, name, genes, kdiff, type_html=file_type)

        if not genotypes_graph:
            return False

        if not file_type:
            return send_file(genotypes_graph, 'application/pdf', 'attachment; filename=genotypes_heatmap.pdf')
        else:
            genotypes_graph = genotypes_graph.split("graphs/")[1]
            template = "temp/display_genotype.html"
            context = {'html_file': genotypes_graph, }
            return render(request, template, context)


    ##########################################################################################################
    #
    #     haplotypes_heatmap - CREATE MULTIPLE HAPLOTYPES HEATMAP AND EXPORT
    #
    ##########################################################################################################

    def haplotypes_heatmap(self, name, samples, haplo_gene, genes, kdiff):
        samples = samples.values_list('name', 'haplotype')
        genes = genes.values_list('name')
        genes = list(chain.from_iterable(genes))
        samples_haplotypes = []

        for samp in samples:
            samp_name = samp[0]
            haplotype = Haplotypes_files.objects.filter(id=samp[1], by_gene=haplo_gene).values_list('file')[0][0]
            samples_haplotypes.append([samp_name , haplotype])

        haplotype_graph = export_genotypes.haplotypes_heatmap(samples_haplotypes, name, genes, kdiff)

        if not haplotype_graph:
            return False

        return send_file(haplotype_graph, 'application/pdf', 'attachment; filename=haplotypes_graph.pdf')


    ##########################################################################################################
    #
    #    ************* ALLELES APPEARS GRAPH *******************************
    #
    ##########################################################################################################

    def alleles_graph(self, name, all_samples, all_alleles, kdiff):
        detail = []

        all_sam = Alleles_Samples.objects.filter(sample__in=all_samples, allele__in=all_alleles, kdiff__gte=kdiff, hap="geno")
        all_s = all_sam.values_list('allele__id', 'patient__id').order_by(SORT_ALLELES_SAMPLES_BY_LOCUS, 'allele__name', 'patient__id')

        gene_patients = all_sam.values_list('allele__gene__name', 'patient__id').order_by(SORT_ALLELES_SAMPLES_BY_LOCUS, 'patient__id')

        # count the number of subjects for each gene
        map_gene_patients = {}
        gene = ""
        pid = 0
        count_patients = 0
        for gp in gene_patients:
            if (gene == gp[0]):
                if (pid != gp[1]):
                    pid = gp[1]
                    count_patients += 1
            else:
                if (gene != ""):
                    map_gene_patients[gene] = count_patients
                count_patients = 1
                gene = gp[0]

        if gene != "":
            map_gene_patients[gene] = count_patients

        all_alleles = all_alleles.values_list('name', 'gene__name', "id").order_by(SORT_ALLELES_BY_LOCUS,'name')
        i = 0
        k = 0
        single_allele_genes = ["Single allele genes"]
        single_allele_genes_names = []
        single_allele_genes_count = []
        while i < len(all_alleles):
            gene = all_alleles[i][1]
            alleles = []

            while gene == all_alleles[i][1]:
                alleles.append(all_alleles[i])
                i += 1
                if (i >= len(all_alleles)):
                    break

            x = []
            y = []
            obj = [gene]

            for allele in alleles:
                allele_name = allele[0]
                patien_id = allele[2]
                patients = []
                pid = -1

                while k < len(all_s):
                    if all_s[k][0] == patien_id:
                        if pid < all_s[k][1]:
                            pid = all_s[k][1]
                            if pid not in patients:
                                patients.append(pid)
                        k += 1
                    else:
                        break

                appear = len(patients)
                if (appear > 0):
                    x.append(allele_name.split('*', 1)[1].upper())
                    y.append(appear)

                elif (not "_" in allele_name) & (not "Del" in allele_name):
                    x.append(allele_name.split('*', 1)[1].upper())
                    y.append(appear)

            if (len(y) > 0) & (gene in map_gene_patients.keys()):
                if len(y) > 1:
                    obj.append(x)
                    obj.append(y)
                    obj.append(map_gene_patients[gene])
                    detail.append(obj)
                else:
                    single_allele_genes_names.append(gene + "\n" + x[0])
                    single_allele_genes_count.append(y[0])


        if (len(y) > 0) & (gene in map_gene_patients.keys()):
            if len(y) > 1:
                obj.append(x)
                obj.append(y)
                obj.append(map_gene_patients[gene])
                detail.append(obj)
            else:
                single_allele_genes_names.append(gene + "\n" + x[0])
                single_allele_genes_count.append(y[0])

        if single_allele_genes_count:
            single_allele_genes.append(single_allele_genes_names)
            single_allele_genes.append(single_allele_genes_count)
            detail.append(single_allele_genes)

        file = exportR_data.output(name, detail)
        return send_file(file, 'application/pdf', 'attachment; filename=allele_distribution.pdf')


    ##########################################################################################################
    #
    #     alleles_usage_graph
    #     SHOWS HOW MANY ALLELES OF EACH GENE HAVE APPEARED
    #
    ##########################################################################################################

    def alleles_usage_graph(self, request, name, all_samples, all_alleles, kdiff, with_patterns):
        # check if to take in count the distinct patterns too.
        if not with_patterns:
            all_alleles = all_alleles.filter(is_single_allele=True)

        all_s = Alleles_Samples.objects.filter(sample__in=all_samples, allele__in=all_alleles, kdiff__gte = kdiff) \
            .values_list('allele__gene__name', 'allele__id', 'allele__gene__type')\
            .order_by(SORT_ALLELES_SAMPLES_BY_SEGMENT, SORT_ALLELES_SAMPLES_BY_LOCUS, 'allele__name')

        i = 0
        genes_alleles = []
        # runs over all alleles_samples list
        while i < len(all_s):
            genes = []
            alleles = []
            type = all_s[i][2]

            gene = all_s[i][0]
            gene_alleles = []

            # run over alleles of the specific gene
            while i < len(all_s):
                if (gene != all_s[i][0]):
                    break

                allele = all_s[i][1]
                gene_alleles.append(allele)
                i += 1

            gene_alleles = set(gene_alleles)
            if with_patterns:
                patterns = Alleles_patterns.objects.filter(allele_in_p__id__in=gene_alleles,
                                                           pattern__id__in=gene_alleles).values_list('pattern__id')
                patterns = set([pattern[0] for pattern in patterns])
                gene_alleles = gene_alleles - patterns

            genes.append(gene.replace(type, ""))
            alleles.append(len(gene_alleles))

            genes_alleles.append([gene, len(gene_alleles)])

        graph_path = graphs.alleles_usage_graph(genes_alleles=genes_alleles, name=name)
        graph_path = graph_path.split("graphs/")[1]

        template = "temp/display_genotype.html"
        context = {'html_file': graph_path, }

        return render(request, template, context)


    ##########################################################################################################
    #
    #     GENE USAGE graph
    #     SHOWS THE FREQUENCIES DISTRIBUTION BETWEEN THE GENES
    #
    ##########################################################################################################

    def freq_dist(self, name, samples, calculate_by_clones, genes):
        samples = list(chain.from_iterable(samples.values_list('id')))

        samples_num = len(samples)
        i = 0
        chunck = 30
        genes_frequencies = defaultdict(list)
        while i < samples_num:
            frequencies = Genes_distribution.objects.filter(sample__id__in=samples[i:min(samples_num, i + chunck)],
                            count_by_clones=calculate_by_clones, gene__in=genes).values_list('sample__id', 'gene__name', 'frequency')
            i += chunck
            for freq in frequencies:
                gene = freq[1]
                usage = float(freq[2])
                genes_frequencies[gene].append(usage)
        file = graphs.freq_dist(genes_frequencies, name=name)
        if file:
            return send_file(file, 'application/pdf', 'attachment; filename=frequencies_distribution.pdf')
        else:
            return False

    ##########################################################################################################
    #
    #     HETEROZYGOUS GRAPH
    #     Shows the distribution of heterozygous & homozygous for each gene.
    #
    ##########################################################################################################

    def hetero_graph(self, request, name, all_samples, all_alleles, kdiff, with_patterns):
        # check if to take in count the distinct patterns too.
        if not with_patterns:
            all_alleles = all_alleles.filter(is_single_allele=True)

        all_s = Alleles_Samples.objects.filter(sample__in=all_samples, allele__in=all_alleles, kdiff__gte=kdiff) \
            .values_list('allele__gene__name', 'patient__id', 'allele__id', 'allele__gene__type', 'allele__name', 'sample__name')\
            .order_by(SORT_ALLELES_SAMPLES_BY_SEGMENT, SORT_ALLELES_SAMPLES_BY_LOCUS, 'patient__id', 'allele__id')

        i = 0
        gene_hetrozygous_dis = []
        while i < len(all_s):
            gene = all_s[i][0]
            ht_count = 0
            hm_count = 0

            # RUN OVER GENE
            while (i < len(all_s)):
                if (gene != all_s[i][0]):
                    break

                patient = all_s[i][1]
                gene_alleles = []
                # RUN OVER SUBJECT ALLELES
                while (i < len(all_s)):
                    if (patient != all_s[i][1]) | (gene != all_s[i][0]):
                        break

                    allele_id = all_s[i][2]
                    gene_alleles.append(allele_id)
                    i += 1

                gene_alleles = set(gene_alleles)

                if with_patterns:
                    patterns = Alleles_patterns.objects.filter(allele_in_p__id__in=gene_alleles,
                                                               pattern__id__in=gene_alleles).values_list('pattern__id')

                    patterns = set([pattern[0] for pattern in patterns])
                    gene_alleles = gene_alleles - patterns

                if len(gene_alleles) > 1:
                    ht_count += 1
                elif len(gene_alleles) > 0:
                    hm_count += 1

            gene_hetrozygous_dis.append([gene, hm_count, ht_count])

        graph_path = graphs.heterozygous_graph(gene_hetrozygous_dis=gene_hetrozygous_dis, name=name)
        graph_path = graph_path.split("graphs/")[1]

        template = "temp/display_genotype.html"
        context = {'html_file': graph_path, }
        return render(request, template, context)


    #############################################################################################################
    #
    #     ********************* GET FUNCTION ********************************
    #
    #############################################################################################################

    def get(self, request):
        # load all forms
        form = self.form_class(request.GET)
        advance = self.advance(request.GET)
        filter_by_gene = self.filter_by_gene(request.GET)
        filter_samples_by_gene = self.filter_samples_by_gene(request.GET)
        haplotype_graph_form = self.haplotype_graph_form(request.GET)

        # ******************* GETTING THE PARAMETERS ***********************************

        name = str(request.GET.get('name', ''))
        reads_number_gte = request.GET.get('reads_number_gte', 0)

        species = request.GET.get('species', "")
        chain = request.GET.get('chain', "")
        tissue = request.GET.get('tissue', "")
        cell_type = request.GET.get('cell_type', "")
        sub_cell_type = request.GET.get('sub_cell_type', "")
        isotype = request.GET.get('isotype', "")

        study = request.GET.getlist('study', None)

        umi = request.GET.get('umi', None)
        helix = request.GET.get('helix', "")
        sequencing_length = request.GET.get('sequencing_length', "")
        sequencing_platform = request.GET.get('sequencing_platform', "")
        primer_3_prime = request.GET.get('primer_3_prime', "")
        primer_5_prime = request.GET.get('primer_5_prime', "")

        patient = request.GET.get('patient', "")
        name_in_paper = request.GET.get('name_in_paper', "")
        sex = request.GET.get('sex', "")
        health = request.GET.get('health_status', "")

        haplotype_by = request.GET.get('haplotype_by', None)

        kdiff = request.GET.get('kdiff', 0)
        genes_type = request.GET.getlist('genes_type', "")
        genes = request.GET.getlist('genes', "")
        with_pseudo_genes = int(request.GET.get('with_pseudo_genes', False))

        alleles_s = request.GET.getlist('alleles_s', "")
        alleles_n = request.GET.getlist('alleles_n', "")

        rows = request.GET.get('rows', 20)

        # ********************  HANDLING WITH POSSIBLE PROBLEMS WITH THE PARAMETERS ************************

        if rows == "":
            rows = 20
        rows = int(rows)

        if umi == "2":
            umi = True
        elif umi == "3":
            umi = False
        else:
            umi = None

        try:
            float(kdiff)
        except:
            kdiff = 0

        if (study == "") | (study == ['']):
            study = None

        if (genes == "") | (genes == ['']):
            genes = None
        if (alleles_s == "") | (alleles_s == ['']):
            alleles_s = None
        if (alleles_n == "") | (alleles_n == ['']):
            alleles_n = None
        if patient == "":
            patient = None

        checked_before = request.GET.getlist('checked_before',[])
        checked = request.GET.getlist('checks')

        uncheck = []
        if checked != checked_before:
            for d in checked_before:
                if d not in checked:
                    uncheck.append(d)

        selected = request.GET.getlist('selects', [])
        selected = list(set().union(selected, checked))
        selected = list(set(selected) - set(uncheck))
        selected = list(map(int, selected))
        act = request.GET.get('action', "")

        if not request.session.session_key:
            request.session.save()
        s_id = request.session.session_key

        try:
            page = max(0, int(request.GET.get('page', 1)) - 1)
        except:
            page = 0

        # ************************************* FILTER SECTION *********************************************************

        if form.is_valid():
            all_samples = Samples.objects.all()
            tissue_pro = Tissue_Pro.objects.all()
            geno_detection = Geno_Detection.objects.all()
            seq_protocol = Seq_Protocols.objects.all()

            if haplotype_by:
                haplo_files = Haplotypes_files.objects.filter(by_gene__icontains=haplotype_by)
                all_samples = all_samples.filter(haplotype__in=haplo_files).distinct()

            all_samples = all_samples.filter(name__icontains=name, geno_detection__in=geno_detection).distinct()
            patients = Patients.objects.all()

            if study:
                all_samples = all_samples.filter(study__in=study)

            if reads_number_gte:
                all_samples = all_samples.filter(row_reads__gte=reads_number_gte)

            # filtering by subject
            if patient:
                all_samples = all_samples.filter(patient=patient)
                patients = patients.filter(id=patient)

            if health:
                patients = patients.filter(status__icontains=health)

            if name_in_paper:
                patients = patients.filter(name_in_paper__icontains=name_in_paper)

            if sex:
                patients = patients.filter(sex__icontains=sex)

            # fitering by tissue
            if cell_type:
                tissue_pro = tissue_pro.filter(cell_type__icontains=cell_type)

            if sub_cell_type:
                tissue_pro = tissue_pro.filter(sub_cell_type__icontains=sub_cell_type)

            if species:
                tissue_pro = tissue_pro.filter(species__icontains=species)

            if isotype:
                tissue_pro = tissue_pro.filter(isotype__iexact=isotype)

            if tissue:
                tissue_pro = tissue_pro.filter(tissue__iexact=tissue)

            if chain:
                all_samples = all_samples.filter(chain__iexact=chain)

            # filtering by sequence procolos
            if helix:
                seq_protocol = seq_protocol.filter(helix=helix)

            if umi:
                seq_protocol = seq_protocol.filter(umi=umi)

            if sequencing_length:
                seq_protocol = seq_protocol.filter(sequencing_length=sequencing_length)

            if sequencing_platform:
                seq_protocol = seq_protocol.filter(sequencing_platform=sequencing_platform)

            if primer_3_prime:
                seq_protocol = seq_protocol.filter(primer_3_prime=primer_3_prime)

            if primer_5_prime:
                seq_protocol = seq_protocol.filter(primer_5_prime=primer_5_prime)

            # filtering by allele
            if alleles_n:
                alleles_s = Alleles.objects.filter(name__in=alleles_n).values_list('pk')
                data = request.GET.copy()
                als = [allele[0] for allele in alleles_s]
                data.setlist('alleles_s', als)
                filter_samples_by_gene = self.filter_samples_by_gene(data)

            if alleles_s:
                all_alleles_s = Alleles.objects.filter(pk__in=alleles_s)
                samples_alleles = Alleles_Samples.objects.filter(allele__in = all_alleles_s).values_list('sample__pk')
                all_samples = all_samples.filter(pk__in=samples_alleles)

            all_samples = all_samples.filter(patient__in=patients)
            all_samples = all_samples.filter(seq_protocol__in=seq_protocol, tissue_pro__in=tissue_pro)
            all_samples = all_samples.distinct()

            # filtering the alleles
            all_genes = Gene.objects.all()
            if "" not in genes_type:
                all_genes = all_genes.filter(type__in=genes_type)

            if genes:
                all_genes = all_genes.filter(pk__in=genes)

            all_genes = all_genes.filter(~Q(name__icontains="or"))

            if not with_pseudo_genes:
                all_genes = all_genes.filter(~Q(name__in=PSEUDO_GENES))

            all_alleles = Alleles.objects.filter(gene__in=all_genes)

            ###########################################################################################
            # ********************** ACTIONS SECTION **************************************************
            ###########################################################################################

            if act.lower() == "submit":
                selected = []

            if act.lower() == "geno":
                geno_type = request.GET.get('geno-type', "heat")
                geno_select = request.GET.get('geno-select', "sample")
                file_type = request.GET.get('genofile-type', "pdf")
                geno_kdiff = request.GET.get('geno-k', 0)

                temp_all_samples = all_samples
                if len(selected) > 0:
                    temp_all_samples = all_samples.filter(pk__in=selected)

                if "individual" in geno_select:
                    selected_subjects = temp_all_samples.values_list("patient__id")
                    selected_subjects = [x[0] for x in selected_subjects]
                    temp_all_samples = Samples.objects.filter(patient__id__in=selected_subjects, samples_group__gte=1)

                if "pdf" in file_type:
                    file_type = False
                else:
                    file_type = True

                if not geno_kdiff:
                    geno_kdiff = 0
                else:
                    geno_kdiff = float(geno_kdiff)

                name = str(s_id) + "genotype-" + geno_type
                if geno_type == "list":
                    if len(temp_all_samples) > 20:
                        messages.error(request, "Error select up to 20 samples.")
                    else:
                        geno_graph = self.genotypes_graph(request, name, temp_all_samples, all_genes, file_type,
                                                          with_pseudo_genes)
                        if geno_graph:
                            return geno_graph
                        messages.error(request, "Error in creating multiple genotypes graph.")
                else:
                    geno_graph = self.genotypes_heatmap(request, name, temp_all_samples, all_genes, geno_kdiff, file_type)
                    if geno_graph:
                        return geno_graph
                    messages.error(request, "Error in creating multiple genotypes graph.")

            if act.lower() == "haplo":
                graph_by_gene = request.GET.get('graph_by_gene', None)
                haplo_select = request.GET.get('haplo_select', None)
                haplo_k = request.GET.get('haplo_k', 0)
                name = str(s_id) + "haplotype-" + graph_by_gene

                temp_all_samples = all_samples
                if len(selected) > 0:
                    temp_all_samples = all_samples.filter(pk__in=selected)

                if "individual" in haplo_select:
                    selected_subjects = temp_all_samples.values_list("patient__id")
                    selected_subjects = [x[0] for x in selected_subjects]
                    temp_all_samples = Samples.objects.filter(patient__id__in=selected_subjects, samples_group__gte=1)

                if not graph_by_gene:
                    messages.error(request, "You must select gene to haplotype by for create haplotype graph.")
                else:
                    haplo_files = Haplotypes_files.objects.filter(by_gene__icontains=graph_by_gene)
                    temp_all_samples = temp_all_samples.filter(haplotype__in=haplo_files).distinct()

                    if temp_all_samples.count():
                        hap_heatmap = self.haplotypes_heatmap(name, temp_all_samples, graph_by_gene, all_genes, haplo_k)
                        if hap_heatmap:
                            return hap_heatmap
                        else:
                            messages.error(request, "Sorry we got error in creating haplotype heatmap.\n")
                            print("Reported error: Sorry we got error in creating haplotype heatmap.\n")
                    else:
                        messages.error(request, "The samples you chose dosen't have haplotype files by: " + graph_by_gene)


            # Gene usage graph
            if act.lower() == "freqs":
                use_all_samples = request.GET.get('freq-dot', 0) == '1' # freq-dot is '0' for single sample per patient, '1' for all samples
                calc_by_clones = request.GET.get('calc-freq', 0) == '0' # calc-freq is '0' for clones, '1' for sequences

                if (selected != []):
                    all_samples = all_samples.filter(pk__in=selected)

                if use_all_samples: # graph_dots = samples. Only one sample from each patient has a non-zero samples_group.
                    all_samples = all_samples.filter(samples_group__gte=0)
                else:
                    all_samples = all_samples.filter(samples_group__gte=1)
                gene_usage_graph = self.freq_dist(s_id, samples=all_samples, calculate_by_clones=calc_by_clones, genes=all_genes)
                if gene_usage_graph:
                    return gene_usage_graph
                else:
                    messages.error(request, "Sorry we got error in creating gene usage graph.\n")
                    print("Reported error: Sorry we got error in creating gene usage graph.\n")

            # graph create
            if act.lower() == "alleles":
                with_ambiguous_alleles = int(request.GET.get('allele-dis-amb', False))
                with_novel_alleles = int(request.GET.get('allele-dis-novel', True))

                if not with_ambiguous_alleles:
                    all_alleles = all_alleles.filter(~Q(name__icontains="_", is_single_allele=False))

                if not with_novel_alleles:
                    all_alleles = all_alleles.filter(~Q(name__icontains="_", is_single_allele=True))


                if (all_samples.exists()) & (all_alleles.exists()):
                    if (selected != []):
                        all_samples = all_samples.filter(pk__in=selected)

                    return self.alleles_graph(s_id, all_samples, all_alleles, kdiff)


            all_alleles = all_alleles.filter(~Q(name__icontains="del"))
            if act.lower() == "hetero":
                with_ambiguous_alleles = int(request.GET.get('hetero-amb', False))
                if (all_samples.exists()) & (all_alleles.exists()):
                    if (selected != []):
                        all_samples = all_samples.filter(pk__in=selected)

                    return self.hetero_graph(request, s_id, all_samples, all_alleles, kdiff,
                                             with_patterns=with_ambiguous_alleles)

            if act.lower() == "gene_alleles":
                with_ambiguous_alleles = int(request.GET.get('usage-amb', False))
                if (all_samples.exists()) & (all_alleles.exists()):
                    if (selected != []):
                        all_samples = all_samples.filter(pk__in=selected)

                    return self.alleles_usage_graph(request, s_id, all_samples, all_alleles, kdiff,
                                                         with_patterns=with_ambiguous_alleles)

            # send to data export
            if act.lower() == "table":
                if (selected != []):
                    return self.export_sample_table(s_id, all_samples.filter(pk__in=selected))
                return self.export_sample_table(s_id, all_samples)

            # ********************** RETURN PAGE **************************************************
            l = int(math.ceil(float(len(all_samples)) / float(rows)))
            pages = list(range(max(1, page - 4), min(page + 7, l)))
            if 1 < page - 4:
                pages = [1] + pages
            if l not in pages:
                pages.append(l)
            prev = min(max(1, page), max(pages))
            next = min(max(pages), max(1, page + 2))
            context = {'all_samples': all_samples[page * rows:page * rows + rows], 'form': form, 'pages': pages,
                       'filter_samples_by_gene': filter_samples_by_gene, 'haplotype_graph_form': haplotype_graph_form,
                       'advance': advance, 'filter_by_gene': filter_by_gene, 'selected': selected, 'next': next, 'prev': prev, 'page': page + 1}
            return render(request, self.template_name, context)

        all_samples = Samples.objects.all()
        selected = []
        l = int(math.ceil(len(all_samples) / 20.0))
        pages = list(range(max(1, page - 4), min(page + 7, l)))
        if 1 < page - 4:
            pages.append(1)
        if l not in pages:
            pages.append(l)
        prev = min(max(1, page), max(pages))
        next = min(max(pages), max(1, page + 2))
        context = {'all_samples': all_samples[page * 20:page * 20 + 20], 'form': form, 'pages':pages,
                   'filter_samples_by_gene': filter_samples_by_gene, 'haplotype_graph_form': haplotype_graph_form,
                   'advance': advance, 'filter_by_gene': filter_by_gene, 'selected': selected, 'next': next, 'prev': prev, 'page': page + 1}
        return render(request, self.template_name, context)

    def post(self, request):
        pass


##########################################################################################################
#
#     ********************* ALLELES PAGE ********************************
#
##########################################################################################################

class alleles_table(View):
    template_name = 'tables/alleles.html'
    form_class = AllelesSearchForm
    #############################################

    #############################################
    def get(self, request):
        if len(request.GET) > 0:
            form = self.form_class(request.GET)
        else:
            form = self.form_class()
        try:
            page = max(0, int(request.GET.get('page', 1)) - 1)
        except:
            page = 0

        # check and if necessary rebuild the novel allele confidence analysis

        check_novel_confidence()

        if form.is_bound and form.is_valid():
            name = form.cleaned_data['name']
            sample_name = form.cleaned_data['sample']
            similar = form.cleaned_data['similar']
            show_imgt = form.cleaned_data['show_imgt']
            show_ambiguous = form.cleaned_data['show_ambiguous']
            show_novel = form.cleaned_data['show_novel']
            show_deletions = form.cleaned_data['show_deletions']
            show_low_confidence = form.cleaned_data['show_low_confidence']

            all_alleles = Alleles.objects.filter(~Q(name__icontains="Del", appears=0)).order_by(SORT_ALLELES_BY_SEGMENT, "name")

            if sample_name is not "":
                samples = Samples.objects.filter(name__icontains=sample_name).values_list('id', flat=True)
                if samples:
                    all_alleles = all_alleles.filter(alleles_samples__sample_id__in=samples).distinct()
                else:
                    all_alleles = Alleles.objects.none()
            else:
                all_alleles = Alleles.objects.filter(~Q(name__icontains="Del", appears=0)).order_by(SORT_ALLELES_BY_SEGMENT, "name")

            if name is not "":
                all_alleles = all_alleles.filter(name__icontains=name)

            if similar is not "":
                all_alleles = all_alleles.filter(similar__icontains=similar)

            if not show_imgt:
                all_alleles = all_alleles.filter(name__regex='^.*(_|Del).*$')

            if not show_ambiguous:
                all_alleles = all_alleles.filter(name__regex='^((?!_[0-9]+).)*$')

            if not show_novel:
                all_alleles = all_alleles.filter(name__regex='^((?!_[acgt][0-9]+[acgt]).)*$')

            if not show_deletions:
                all_alleles = all_alleles.filter(name__regex='^((?!Del).)*$')

            if not show_low_confidence:
                all_alleles = all_alleles.exclude(low_confidence=True)

        else:
            all_alleles = Alleles.objects.filter(~Q(name__icontains="Del", appears=0)).order_by(SORT_ALLELES_BY_SEGMENT, "name")

        if 'download' in request.GET:
            recs = []
            for allele in all_alleles:
                if allele.seq is not None and len(allele.seq) > 0:
                    recs.append(SeqRecord(Seq(allele.seq, generic_dna), id=allele.name, description="Occurrences=%d" % allele.appears))
                else:
                    recs.append(SeqRecord(Seq('.', generic_dna), id=allele.name, description="Occurrences=%d" % allele.appears))

            sh = StringIO()
            SeqIO.write(recs, sh, "fasta")
            resp = HttpResponse(sh.getvalue())
            resp['Content-Type'] = 'text/plain'
            resp['Content-Disposition'] = 'attachment; filename=alleles.fa'
            return resp

        l = math.ceil(len(all_alleles) / 20.0)
        pages = list(range(max(1, page - 4), min(page + 7, l)))
        if 1 < page - 4:
            pages.append(1)
        if l not in pages:
            pages.append(l)
        pages.sort()
        prev = min(max(1, page), max(pages))
        next = min(max(pages), max(1, page + 2))
        context = {'all_alleles': all_alleles[page * 20:page * 20 + 20], 'form': form, 'pages': pages,
                   'next': next, 'prev': prev, 'page': page + 1, }
        return render(request, self.template_name, context)


##########################################################################################################
#
#     ********************* EXPLORE PAGE ********************************
#
##########################################################################################################

class explore(View):
    template_name = 'explore.html'

    def get(self, request):
        #count2 = Samples.objects.all().values('study__name').annotate(count=Count('study__name'))
        SexCount = Patients.objects.all().values('sex').annotate(count=Count('sex'))
        Sex_list = [[item['sex'], item['count'] ]
         for item in SexCount.all() ]

        StudyCount = Samples.objects.all().values('study__name').annotate(count=Count('study__name'))
        Study_list = [[item['study__name'], item['count'] ]
         for item in StudyCount.all() ]

        DiagnosisCount = Patients.objects.all().values('status').annotate(count=Count('status'))
        Diagnosis_list = [[item['status'], item['count'] ]
         for item in DiagnosisCount.all() ]

        CelltypeCount = Samples.objects.all().values('tissue_pro__sub_cell_type').annotate(count=Count('tissue_pro__sub_cell_type'))
        Celltype_list = [[item['tissue_pro__sub_cell_type'], item['count'] ]
         for item in CelltypeCount.all() ]

        TissueCount = Samples.objects.all().values('tissue_pro__tissue').annotate(count=Count('tissue_pro__tissue'))
        Tissue_list = [[item['tissue_pro__tissue'], item['count']]
                         for item in TissueCount.all()]

        allstudies = Studies.objects.all()

        context = {'allstudies': allstudies, 'SexCount': json.dumps(list(SexCount)), 'Sex_list':json.dumps(list(Sex_list)), 'Study_list':Study_list, 'Diagnosis_list':Diagnosis_list, 'Celltype_list':Celltype_list, 'Tissue_list':Tissue_list   }
        return render(request, self.template_name,context=context)

##########################################################################################################
#
#     ********************* LICENSING PAGE ********************************
#
##########################################################################################################

class licensing(View):
    template_name = 'licensing.html'

    def get(self, request):
        return render(request, self.template_name)


##########################################################################################################
#
#     *************************** SHOWING GRAPHS PAGE ********************************
#                               DISPLAY 3 GRAPHS OVER ROW
#
##########################################################################################################

class presenting_graph(View):
    def get(self, request):
        temp = request.get_full_path()
        template = temp.split("/Graph/")[1]
        template = parse.unquote(template)
        template = os.path.join(SCRATCH_ROOT, 'graphs', template)
        if '.css' in template:
            d = 'text/css'
        elif '.js' in template:
            d = 'application/javascript'
        else:
            d = 'text/html'
        return send_file(template, d, 'inline')


##########################################################################################################
#
#     ********************* GUIDE PAGE ********************************
#
##########################################################################################################

class guide(View):
    template_name = 'user_guide.html'

    def get(self, request):
        return render(request, self.template_name)


##########################################################################################################
#
#     download genotype table
#
##########################################################################################################

def down_geno(self, sample_name):
    sample = Samples.objects.filter(name=sample_name)[0]
    path = os.path.join(BASE_DIR, sample.genotype.path)
    return send_file(path, 'text/tab-separated-values', 'attachment; filename=' + sample_name + '.tab')


##########################################################################################################
#
#     displaying genotype graph
#
##########################################################################################################

def genotype_graph(request, sample_name, html):
    html = int(html)

    sample = Samples.objects.filter(name=sample_name)[0]
    genotype_path = os.path.join(BASE_DIR, sample.genotype.path)
    genotype_path = export_genotypes.personal_genotype(sample_name, genotype_path, html)


    if not genotype_path:
        template = "temp/Error_message.html"
        messages.error(request, "Sorry couldn't create the personal genotype graph for " + sample_name)
        print("Reported error: Sorry couldn't create the personal genotype graph for " + sample_name)
        return render(request, template)

    if html:
        genotype_path = genotype_path.split("graphs/")[1]
        template = "temp/display_genotype.html"
        context = {'html_file': genotype_path, }
        return render(request, template, context)

    else:
        return send_file(genotype_path, 'application/pdf', 'attachment; filename=' + sample_name + '_genotype.pdf')


##########################################################################################################
#
#     displaying haplotype graph
#
##########################################################################################################

def haplotype_graph(request, id, html):
    html = int(html)
    haplotype = Haplotypes_files.objects.filter(id=id)
    sample = Samples.objects.filter(haplotype__in=haplotype)[0]
    haplotype = haplotype[0]

    haplotype_path = os.path.join(BASE_DIR, haplotype.file.path)
    haplo_name = haplotype.file.name.split(".")[0].split("/")[-1]
    title = sample.name + '-' + haplotype.by_gene
    haplotype_path = export_genotypes.personal_haplotype(title, haplo_name, haplotype_path, html)

    if not haplotype_path:
        template = "temp/Error_message.html"
        messages.error(request, "Sorry couldn't create the personal haplotype graph: " + title)
        print("Reported error: Sorry couldn't create the personal haplotype graph: " + title)
        return render(request, template)

    if html:
        haplotype_path = haplotype_path.split("graphs/")[1]
        template = "temp/display_genotype.html"
        context = {'html_file': haplotype_path, }
        return render(request, template, context)

    else:
        return send_file(haplotype_path, 'application/pdf', 'attachment; filename=' + sample.name + '-' + haplotype.by_gene + '.pdf')


##########################################################################################################
#
#     download genotype stats
#
##########################################################################################################

def down_geno_stats(self, sample_name):
    sample = Samples.objects.filter(name=sample_name)[0]
    return send_file(os.path.join(BASE_DIR, sample.genotype_stats.path), 'text/csv', 'attachment; filename=' + sample.name + '_stats.csv')



##########################################################################################################
#
#     download haplotype table
#
##########################################################################################################

def down_haplotype(self, id):
    haplotype = Haplotypes_files.objects.filter(id=id)
    sample = Samples.objects.filter(haplotype__in=haplotype)[0]
    haplotype=haplotype[0]
    return send_file(os.path.join(BASE_DIR, haplotype.file.path), 'text/tab-separated-values', 'attachment; filename=' + sample.name + '-' + haplotype.by_gene + '.tab')

