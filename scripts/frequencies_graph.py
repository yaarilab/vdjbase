import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from itertools import chain

from constant_args import *
import os, sys, sqlite3

def output(samples):

    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHV'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighv = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHD'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighd = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHJ'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighj = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT sample_id, gene_id, frequency FROM database_genes_distribution WHERE sample_id IN (%s)" %','.join('?'*len(samples)), samples)
    frequncies = c.fetchall()
    c.close
    ighv = []
    ighd = []
    ighj = []

    for freq in frequncies:
        if int(freq[1]) in map_ighv:
            ighv.append([freq[0],map_ighv[freq[1]].replace("IGHV", ""),freq[2]])
        elif int(freq[1]) in map_ighd:
            ighd.append([freq[0],map_ighd[freq[1]].replace("IGHD", ""),freq[2]])
        elif int(freq[1]) in map_ighj:
            ighj.append([freq[0],map_ighj[freq[1]].replace("IGHJ", ""),freq[2]])
    labels = ['sid','gene', 'frequency']

    ighv_frequencies = pd.DataFrame.from_records(ighv, columns=labels)
    ighd_frequencies = pd.DataFrame.from_records(ighd, columns=labels)
    ighj_frequencies = pd.DataFrame.from_records(ighj, columns=labels)

    fig = plt.figure()
    fig.set_size_inches(37, 19)
    sns.set(font_scale=1.8)

    ax1 = plt.subplot(212)
    ax1 = ax1.margins(1)
    ighv_plot = sns.stripplot(x="gene", y="frequency", data=ighv_frequencies, linewidth=0.1, ax=ax1)
    ighv_plot.set_xticklabels(ighv_plot.get_xticklabels(),rotation=90)
    ighv_plot.set_title('IGHV', size=25)
    ighv_plot.set_ylim(0, max(ighv_frequencies.frequency)*1.2)

    ax2 = plt.subplot(221)
    # the size of A4 paper
    ighd_plot = sns.stripplot(x="gene", y="frequency", data=ighd_frequencies, linewidth=0.1, ax=ax2)
    ighd_plot.set_xticklabels(ighd_plot.get_xticklabels(),rotation=90)
    ighd_plot.set_title('IGHD', size=28)
    ighd_plot.set_ylim(0,max(ighd_frequencies.frequency)*1.2)

    ax3 = plt.subplot(222)
    # the size of A4 paper
    ighj_plot = sns.stripplot(x="gene", y="frequency", data=ighj_frequencies, linewidth=0.1, ax=ax3)
    ighj_plot.set_xticklabels(ighj_plot.get_xticklabels(),rotation=90)
    ighj_plot.set_title('IGHJ', size=25)
    ighj_plot.set_ylim(0,max(ighj_frequencies.frequency)*1.2)


    plt.tight_layout()
    fig.savefig('output.png')


def main(argv):
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHV'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighv = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHD'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighd = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT name, id FROM database_gene WHERE type = 'IGHJ'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_ighj = {int(y[1]):x for (x, y) in zip(all_genes_names, all_genes)}

    c.execute("SELECT sample_id, gene_id, frequency FROM database_genes_distribution")
    frequncies = c.fetchall()
    c.close
    ighv = []
    ighd = []
    ighj = []

    for freq in frequncies:
        if int(freq[1]) in map_ighv:
            ighv.append([freq[0],map_ighv[freq[1]].replace("IGHV", ""),freq[2]])
        elif int(freq[1]) in map_ighd:
            ighd.append([freq[0],map_ighd[freq[1]].replace("IGHD", ""),freq[2]])
        elif int(freq[1]) in map_ighj:
            ighj.append([freq[0],map_ighj[freq[1]].replace("IGHJ", ""),freq[2]])
    labels = ['sid','gene', 'frequency']

    ighv_frequencies = pd.DataFrame.from_records(ighv, columns=labels)
    ighd_frequencies = pd.DataFrame.from_records(ighd, columns=labels)
    ighj_frequencies = pd.DataFrame.from_records(ighj, columns=labels)

    ## colors
    my_pal = {}
    j_pal = {"1": "deepskyblue", "2": "darkorange", "3": "forestgreen", "4": "red", "5": "mediumslateblue", "6": "saddlebrown",}

    for gene in (IGHV_ORDER+IGHD_ORDER):
        if gene[:2] == "1-":
            my_pal[gene] = "deepskyblue"
        if gene[:2] == "2-":
            my_pal[gene] = "darkorange"
        if gene[:2] == "3-":
            my_pal[gene] = "forestgreen"
        if gene[:2] == "4-":
            my_pal[gene] = "red"
        if gene[:2] == "5-":
            my_pal[gene] = "mediumslateblue"
        if gene[:2] == "6-":
            my_pal[gene] = "saddlebrown"
        if gene[:2] == "7-":
            my_pal[gene] = "pink"

    fig = plt.figure()
    fig.set_size_inches(37, 19)
    sns.set(font_scale=1.8)
    sns.set_style("whitegrid")

    ax1 = plt.subplot(212)
    ax1 = ax1.margins(1)
    ighv_plot = sns.stripplot(x="gene", y="frequency", data=ighv_frequencies, jitter=True, order = IGHV_ORDER, linewidth=0.1, ax=ax1, palette=my_pal)
    ighv_plot = sns.boxplot(x="gene", y="frequency", data=ighv_frequencies, order = IGHV_ORDER, color=".3", showfliers=False, ax=ax1)
    ighv_plot.set_xticklabels(ighv_plot.get_xticklabels(),rotation=90)
    ighv_plot.set_title('IGHV', size=25)
    ighv_plot.set_ylim(0, max(ighv_frequencies.frequency)*1.2)

    ax2 = plt.subplot(221)
    # the size of A4 paper
    ighd_plot = sns.stripplot(x="gene", y="frequency", data=ighd_frequencies, order =IGHD_ORDER, jitter=True, linewidth=0.1, ax=ax2, palette=my_pal)
    ighd_plot = sns.boxplot(x="gene", y="frequency", data=ighd_frequencies, order =IGHD_ORDER, color=".3", showfliers=False, ax=ax2)
    ighd_plot.set_xticklabels(ighd_plot.get_xticklabels(),rotation=90)
    ighd_plot.set_title('IGHD', size=28)
    ighd_plot.set_ylim(0,max(ighd_frequencies.frequency)*1.2)

    ax3 = plt.subplot(222)
    # the size of A4 paper
    ighj_plot = sns.stripplot(x="gene", y="frequency", data=ighj_frequencies, jitter=True, linewidth=0.1, ax=ax3, palette= j_pal)
    ighj_plot = sns.boxplot(x="gene", y="frequency", data=ighj_frequencies, color=".3", showfliers=False, ax=ax3)
    ighj_plot.set_xticklabels(ighj_plot.get_xticklabels(),rotation=90)
    ighj_plot.set_title('IGHJ', size=25)
    ighj_plot.set_ylim(0,max(ighj_frequencies.frequency)*1.2)

    plt.tight_layout()
    fig.savefig('output1.png')


if __name__ == "__main__":
    main(sys.argv[1:])