#!/usr/bin/python
import sqlite3
import os, math, re
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
GENE_COLUMN = "GENE"
COUNTS_COLUMN = "COUNTS"
TOTAL_COLUMN = "TOTAL"

GENOTYPE_KDIFF_COLUMN = "K_DIFF"
GENOTYPED_ALLELES_COLUMN = "GENOTYPED_ALLELES"

FREQ_BY_SEQ = "Freq_by_Seq"
FREQ_BY_CLONE = "Freq_by_Clone"


HAPLO_KDIFF_COLUMN = "K"

########################################################################################################################
#
# IN CASE OF RECOGNIZING NEW ALLELES
#
########################################################################################################################

def new_allele(allele, conn):
    c = conn.cursor()

    query_select_name = "SELECT seq, gene_id FROM database_alleles WHERE name  = ?"
    query_select_similar = "SELECT seq, gene_id FROM database_alleles WHERE similar LIKE ?"
    c.execute(query_select_name, (allele.split("_")[0],))
    seq = c.fetchall()
    if seq == []:
        c.execute(query_select_similar, ("%|" + allele.split("_")[0] + "|%",))
        seq = c.fetchall()

    if seq == []:
        allele1_name = allele.split("*")[0] + "D*" + allele.split("*")[1].split("_")[0]
        c.execute(query_select_name, (allele1_name,))
        seq = c.fetchall()

    if seq == []:
        c.execute(query_select_similar, ("%|" + allele1_name + "|%",))
        seq = c.fetchall()

    allele_pattern = re.compile("^[0-9]{2}$")
    mut_pattern = re.compile("^[A,G,T,C,a,g,t,c][0-9]+[A,G,T,C,a,g,t,c]$")
    gid = seq[0][1]
    seq = seq[0][0]
    seq = "".join(seq.split())

    final_allele_name = allele.split("_")[0]
    rep = allele.lower().split("_")[1:]
    ambiguous_alleles = [allele.split("*")[1].split("_")[0]]
    for r in rep:
        if re.search(allele_pattern, r):
            if r not in ambiguous_alleles:
                final_allele_name += "_" + r
                allele_seq_1 = seq
                allele2_name = allele.split("*")[0] + "*" + r # r = allele for example 01
                c.execute(query_select_name, (allele2_name,))
                allele_seq_2 = c.fetchall()
                if allele_seq_2 == []:
                    c.execute(query_select_similar, ("%|" + allele2_name + "|%",))
                    allele_seq_2 = c.fetchall()

                if allele_seq_2 == []:
                    allele2_name = allele.split("*")[0] + "D*" + r # r = allele for example 01
                    c.execute(query_select_name, (allele2_name,))
                    allele_seq_2 = c.fetchall()

                if allele_seq_2 == []:
                    c.execute(query_select_similar, ("%|" + allele2_name + "|%",))
                    allele_seq_2 = c.fetchall()

                allele_seq_2 = allele_seq_2[0][0]
                seq = ""
                length_diff = len(allele_seq_1) - len(allele_seq_2)

                if length_diff > 0:
                    allele_seq_1 += "n"*length_diff
                elif length_diff < 0:
                    allele_seq_2 += "n" * (length_diff * -1)

                for nuc1, nuc2 in zip(allele_seq_1, allele_seq_2):
                    if nuc1 == nuc2:
                        seq += nuc1
                    else:
                        seq += "n"

            ambiguous_alleles.append(r)

        elif re.search(mut_pattern, r):
            final_allele_name += "_" + r
            place = int(r[1:][:len(r) - 2]) - 1
            seq = seq[:place] + r[len(r) - 1] + seq[place + 1:]

    c.execute("SELECT similar, name, id FROM database_alleles WHERE seq  = ?", (seq,))
    same_seq = c.fetchall()

    allele_id = False
    if same_seq != []:
        temp = same_seq[0][1]
        sim = same_seq[0][0]
        if (temp == final_allele_name):
            allele_id = same_seq[0][2]
        else:
            if sim is not None:
                if (final_allele_name in sim):
                    allele_id = same_seq[0][2]
                else:
                    sim = sim + ", |" + allele + "|"
            else:
                sim = "|" + allele + "|"

        if not allele_id:
            c.execute("UPDATE database_alleles SET similar = ? WHERE seq = ?", (sim, seq,))
            c.execute("SELECT id FROM database_alleles WHERE seq = ?", (seq,))
            allele_id = c.fetchall()[0][0]

    elif not allele_id:
        query = "INSERT INTO database_alleles (name, seq, seq_len, gene_id, is_single_allele, appears) VALUES (?, ?, ?, ?, ?, ?)"
        values = (final_allele_name, seq, len(seq), gid, len(ambiguous_alleles)==1, 0)
        c.execute(query, values)
        allele_id = c.lastrowid

    conn.commit()
    c.close()

    return allele_id

########################################################################################################################
#
# ADD ALLELE TO Sample
#
########################################################################################################################


def add2sample (allele, sample_id, haplo, pid, kdiff, conn):
    c = conn.cursor()
    query_get_allele_id_by_name = "SELECT id FROM database_alleles WHERE name  = ?"
    query_get_allele_id_by_similar= "SELECT id FROM database_alleles WHERE similar LIKE ?"
    query_check_dup = "SELECT id FROM database_alleles_samples WHERE allele_id = ? AND sample_id = ? AND hap = ?"
    query_insert_alleles_sam = "INSERT INTO database_alleles_samples (allele_id, sample_id, hap, patient_id, kdiff) " \
                               "VALUES (?, ?, ?, ?, ?)"

    kdiff = float(kdiff)
    if math.isnan(kdiff):
        kdiff = 0.0

    c.execute(query_get_allele_id_by_name, (allele,))
    allele_id = c.fetchall()
    # check if allele exist
    if allele_id != []:
        allele_id = allele_id[0][0]
        values = (allele_id, sample_id, haplo, pid, kdiff)

    else:
        c.execute(query_get_allele_id_by_similar, ("%|" + allele + "|%",))
        allele_id = c.fetchall()
        # check if allele is similar
        if allele_id != []:
            allele_id = allele_id[0][0]
            values = (allele_id, sample_id, haplo, pid, kdiff)

        else:
            # inserting the allele
            allele_id = new_allele(allele, conn)
            values = (allele_id, sample_id, haplo, pid, kdiff)

    c.execute(query_check_dup, (allele_id, sample_id, haplo))
    if c.fetchall() == []:
        c.execute(query_insert_alleles_sam, values)

    conn.commit()
    c.close()


########################################################################################################################
#
#    ADD ALLELES TO SAMPLE ACCORDING TO THE GENOTYPE
#    AND FREQUENCIES
#
########################################################################################################################

def sample_genotype(inputfile, sample_id):
    """
    Upload genotype to sample.
    :param inputfile: the path to the genotype
    :param sample_id: sample id
    """
    haplo = "geno"

    inputfile = os.path.join(BASE_DIR, "uploads/", inputfile)
    genotype = pd.read_csv(inputfile, sep="\t")

    # Establish a sqlite3 connection
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    # getting the pid
    c.execute("SELECT patient_id FROM database_samples WHERE id  = ?", (sample_id,))
    temp = c.fetchall()[0]
    patient_id = temp[0]

    for index, row in genotype.iterrows():
        gene = row[GENE_COLUMN]
        kdiff = row[GENOTYPE_KDIFF_COLUMN]

        for index, allele in enumerate(row[GENOTYPED_ALLELES_COLUMN].split(",")):
            if (allele == "Unk") | ("NR" in allele):
                continue
            elif len(str(allele)) == 1:
                allele = "0" + str(allele)
            elif ("del" in allele.lower()):
                allele = "Del"

            # check if the allele exist in the genotype according to the clone size
            if allele != "Del":
                clone_size = int(row[FREQ_BY_CLONE].split(",")[index])
                if not clone_size:
                    continue

            full_allele_name = gene + "*" + allele
            add2sample(full_allele_name, sample_id, haplo, patient_id, kdiff, conn)

    conn.commit()
    c.close()



########################################################################################################################
#
#    ADD ALLELES TO SAMPLE ACCORDING TO THE HAPLOTYPE
#
########################################################################################################################

def sample_haplotype(inputfile, sid, by_alleles, conn=None):
    """
    Upload haplotype and connect it to sample.
    :param inputfile: the path to the genotype
    :param sid: sample id
    :param by_alleles: the alleles that the haplotype done by.
    """

    inputfile = os.path.join(BASE_DIR, "uploads/", inputfile)
    hapotype = pd.read_csv(inputfile, sep="\t")

    # Establish a sqlite3 connection
    if conn == None:
        conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    # getting the pid
    c.execute("SELECT patient_id FROM database_samples WHERE id  = ?", (sid,))
    temp = c.fetchall()[0]
    pid = temp[0]

    for index, haplo_name in enumerate(by_alleles):
        haplo_index = str(index + 1)
        for _, row in hapotype.iterrows():
            gene = row[GENE_COLUMN]
            kdiff = row[HAPLO_KDIFF_COLUMN + haplo_index]

            for allele in row[haplo_name].split(","):
                if (allele == "Unk") | ("NR" in allele):
                    continue
                elif len(str(allele)) == 1:
                    allele = "0" + str(allele)
                elif ("del" in allele.lower()):
                    allele = "Del"

                full_allele_name = gene + "*" + allele
                add2sample(full_allele_name, sid, haplo_name, pid, kdiff, conn)

    conn.commit()
    c.close()


########################################################################################################################
#
# MAIN
#
########################################################################################################################

def main(argv):
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT id, geno_detection_id, genotype FROM database_samples")
    samples = c.fetchall()
    for s in samples:
        sid = s[0]
        det_id = s[1]
        f = s[2]
        c.execute("SELECT detction FROM database_geno_detection WHERE id = ?", (det_id,))
        align = c.fetchall()[0][0]
        c.execute("SELECT * FROM database_alleles_samples WHERE sample_id = ?", (sid,))
        a = c.fetchall()
        if not a:
            sample_genotype(f, sid)
    c.close()

if __name__ == "__main__":
    # main(sys.argv[1:])
    sample_haplotype("/home/aviv/HC_21_BB_haplo_current_ver.tab", 3413, ["IGHJ6_02","IGHJ6_03"])
