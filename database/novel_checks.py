##########################################################################################################
#
#       Confidence checks on novel alleles
#
#       This might be better as part of the pipeline, but, at least while the checks are under development,
#       they are run when the alleles view is rendered for the first time, so that they can be modified
#       without re-running the pipeline. To re-run the checks, run the command below from sqlite command line
#       UPDATE database_alleles SET novel=False;
#
##########################################################################################################
import sys
import os
from django.db import transaction
from django.db.models import Max

from Bio.Seq import Seq
from Bio.Data.CodonTable import TranslationError

from .models import *
import re
import csv

from website.settings import BASE_DIR


def check_novel_confidence():
    # check if we need to rebuild
    if Alleles.objects.filter(novel=True).count():
        return

    novels = gather_novel_data()
    gather_haplo_data(novels)

    #   Check for deletion on other chromosome, or novel allele present on both chromosomes
    check_for_singleton_infs(novels)

    #   Check for zXXX, XzXX, XXzX, XXXz -> XXXX (where X and z are any base)
    check_XXXX(novels)

    #   Check for an amino acid at a location that has not been previously observed in that family
    check_novel_aa(novels)

    #   Check for RGYW/WRCY hotspot change
    check_hotspot(novels)

    #   Check for FR1 SNP in a sample with FR1 primers
    check_snp_in_short(novels)

    #   Check for SNP in the same location in >1 novel allele from the same sample
    check_shared_snp(novels)

    # Produce some stats on CNV
    # allele_copy_stats()


# Collect info on novel inferences, store in Alleles table
def gather_novel_data():
    AlleleConfidenceReport.objects.all().delete()
    SNPs.objects.all().delete()
    Alleles.objects.all().update(novel=False, low_confidence=False)
    novels = Alleles.objects.exclude(name__regex='^((?!_[acgt][0-9]+[acgt]).)*$')
    novels.update(novel=True)
    novels.update(max_kdiff=0.0)
    for novel in novels:
        novel.max_kdiff = Alleles_Samples.objects.filter(allele=novel).aggregate(Max('kdiff'))['kdiff__max']
        closest_ref = Alleles.objects.filter(name=novel.name.split('_')[0]).first()

        if not closest_ref:
            report_issue(novel, 'No Closest Ref',
                         'The closest reference allele, %s, is not in the database' % (novel.name.split('_')[0]))
        elif not closest_ref.seq:
            report_issue(novel, 'No Ref Sequence',
                         'The closest reference allele, %s, has no defined sequence' % (novel.name.split('_')[0]))
        else:
            novel.closest_ref = closest_ref
            novel.save()

        snps = novel.name.split('_')

        with transaction.atomic():
            for snp in snps:
                if snp[0].isalpha() and snp[-1].isalpha():
                    s = SNPs(from_base=snp[0], pos=int(snp[1:-1]), to_base=snp[-1], allele=novel)
                    s.save()
    return novels

# temp function to look for gene duplication
def allele_copy_stats():
    samples = Samples.objects.all()
    HaplotypeEvidence.objects.all().delete()

    allele_report_samples = {}

    for sample in samples:
        if sample.seq_protocol.sequencing_length == 'Full':
            for haplo_file in sample.haplotype.all():
                with open(os.path.join(BASE_DIR, 'uploads', haplo_file.file.name), 'r', newline='') as fi:
                    reader = csv.reader(fi, dialect='excel-tab')
                    header = True
                    for row in reader:
                        if header:
                            header = False
                            continue
                        h1 = set(row[3].split(',')) - {'Unk', 'Del'}
                        h2 = set(row[4].split(',')) - {'Unk', 'Del'}
                        contains_novel = False
                        for haplotyped in (h1, h2):
                            for allele in haplotyped:
                                if '_' in allele:
                                    contains_novel = True
                                    break
                            if not contains_novel and len(haplotyped) > 0:
                                if row[2] not in allele_report_samples:
                                    allele_report_samples[row[2]] = {}
                                if sample.name not in allele_report_samples[row[2]]:
                                    allele_report_samples[row[2]][sample.name] = len(haplotyped)
                                else:
                                    allele_report_samples[row[2]][sample.name] = max(allele_report_samples[row[2]][sample.name], len(haplotyped))

    allele_report = {}
    allele_report_samples3 = {}
    for gene, counts in allele_report_samples.items():
        totals = [0]*5
        for sample, count in counts.items():
            totals[count] += 1
            if count > 2:
                if gene not in allele_report_samples3:
                    allele_report_samples3[gene] = []
                allele_report_samples3[gene].append(sample)
        allele_report[gene] = totals

    with open('allele_report.csv', 'w') as fo:
        for k in sorted(allele_report.keys()):
            fo.write("%s,%s\n" % (k, ', '.join([str(x) for x in allele_report[k]])))

    with open('allele_report_samples.csv', 'w') as fo:
        for k in sorted(allele_report_samples3.keys()):
            fo.write("%s,%s\n" % (k, ', '.join(allele_report_samples3[k])))




# Collect haplotyping info on novel alleles, store in HaplotypeEvidence
def gather_haplo_data(novels):
    samples = Samples.objects.all()
    HaplotypeEvidence.objects.all().delete()

    for sample in samples:
        for haplo_file in sample.haplotype.all():
            with transaction.atomic():
                with open(os.path.join(BASE_DIR, 'uploads', haplo_file.file.name), 'r', newline='') as fi:
                    reader = csv.reader(fi, dialect='excel-tab')
                    header = True
                    for row in reader:
                        if header:
                            header = False
                            continue
                        haplotyped = set(row[3].split(',')) ^ set(row[4].split(','))
                        for allele in haplotyped:
                            if '_' in allele:
                                n = novels.filter(name=row[2] + '*' + allele.lower()).first()
                                if n:
                                    allele_names = row[5].split(',')
                                    allele_counts = [row[8], row[10], row[12], row[14]]
                                    counts = []

                                    for i in range(len(allele_names)):
                                        counts.append('%s (%s)' % (allele_names[i], allele_counts[i]))

                                    he = HaplotypeEvidence(hap_gene=haplo_file.by_gene, sample=sample, allele=n, counts=', '.join(counts))
                                    he.save()
    for novel in novels:
        if novel.haplotypeevidence_set.count():
            detects = []
            for sample_id in set([h.sample_id for h in novel.haplotypeevidence_set.all()]):

                hap_genes = []
                for h in novel.haplotypeevidence_set.filter(sample_id=sample_id).all():
                    hap_genes.append('%s (%s)' % (h.hap_gene, h.counts))

                detects.append('%s(%s)' % (Samples.objects.filter(id=sample_id).first().name, ', '.join(hap_genes)))

            report_issue(novel, 'Confirmation from Haplotype', 'Inferred allele on one chromosome only in sample(s) %s.' % (', '.join(detects)), low_confidence=False)

def check_XXXX(novels):
    for novel in novels:
        if novel.closest_ref:
            ref_nt = novel.closest_ref.seq.replace('.', '')
            seq_nt = novel.seq.replace('.', '')

            # walk up each identified repeat of 4nt or more, flag any differences
            seq_qpos = [m.start() for m in re.finditer('(.)\\1+\\1+\\1+', str(seq_nt))]
            q_runs = []

            for p in seq_qpos:
                rep_c = seq_nt[p]
                i = p
                while i < len(seq_nt) and i < len(ref_nt) and seq_nt[i] == rep_c:
                    if ref_nt[i] != rep_c and rep_c != 'n':
                        r_pos = find_gapped_index(i, novel.closest_ref.seq)
                        q_runs.append("%d%s%d" % (r_pos, seq_nt[p:p+4], r_pos+3))
                        break
                    i += 1

            if len(q_runs):
                report_issue(novel, 'Repeated Read', 'Possible repeated read error(s) at %s' % (', '.join(q_runs)))


def check_novel_aa(novels):
    ref_vh_codon_usage = get_ref_vh_codon_usage()
    for novel in novels:
        try:
            q_codons = []
            ref_aa_gapped = Seq(novel.closest_ref.seq.upper()).translate(gap='.')
            seq_aa_gapped = Seq(novel.seq.upper()).translate(gap='.')

            family = find_family(novel.closest_ref.name)

            for i in range( min(len(ref_aa_gapped), len(seq_aa_gapped))):
                # if ref_aa_gapped[i] != seq_aa_gapped[i] and '*' not in (ref_aa_gapped[i], seq_aa_gapped[i]) and '.' not in (ref_aa_gapped[i], seq_aa_gapped[i]):
                if ref_aa_gapped[i] != seq_aa_gapped[i] and seq_aa_gapped[i] not in ['*', 'X']:
                    if seq_aa_gapped[i] not in ref_vh_codon_usage[family][i+1]:
                        q_codons.append("%s%d" % (seq_aa_gapped[i], i+1))

            if len(q_codons) > 0:
                report_issue(novel, 'Novel AA', 'Amino Acid(s) previously unreported in this family - %s' % ", ".join(q_codons), low_confidence=len(q_codons) > 1)

        except TranslationError:
            print('Error translating sequence %s or %s: %s' % (novel.name, novel.closest_ref.name, sys.exc_info()[1]))


def check_hotspot(novels):
    for novel in novels:
        ref_nt = novel.closest_ref.seq.replace('.', '')
        seq_nt = novel.seq.replace('.', '')
        ref_qpos = [m.start() for m in re.finditer('[ag][g][ct][at]', ref_nt)]

        q_hotspots= []

        for p in ref_qpos:
            if seq_nt[p+1] == 'c':
                q_hotspots.append("%s%d%s" % (ref_nt[p+1], find_gapped_index(p+1, novel.closest_ref.seq), seq_nt[p+1]))

        ref_qpos = [m.start() for m in re.finditer('[at][ag][c][ct]', ref_nt)]

        for p in ref_qpos:
            if seq_nt[p+2] == 'g':
                q_hotspots.append("%s%d%s" % (ref_nt[p+2], find_gapped_index(p+2, novel.closest_ref.seq), seq_nt[p+2]))

        if len(q_hotspots) > 0:
            report_issue(novel, 'Hotspot SNP', "G/C SNP in RGYW/WRCY hotspot(s) - %s" % ", ".join(q_hotspots), low_confidence=False)


def check_snp_in_short(novels):
    short_studies_only = novels.exclude(samples__seq_protocol__sequencing_length='Full')

    for novel in short_studies_only:
        min_snp = novel.snps_set.all().order_by('pos').first()
        if min_snp.pos <= 40:
            samples = set([s.name.split('_')[0] for s in novel.samples.all()])
            report_issue(novel, 'Possible SNP in primer region', 'Sequence found only in short-read study/studies %s has SNP %s%d%s in first 40 nucleotides' % (
                ', '.join(samples),
                min_snp.from_base, min_snp.pos, min_snp.to_base
            ))


def check_shared_snp(novels):
    positions = {}

    for novel in novels:
        positions[novel.name] = set(['%d%s' % (s.pos, s.to_base) for s in novel.snps_set.all()])

    for novel in novels:
        novel_positions = positions[novel.name]
        issues = []

        for sample in novel.samples.all():
            shared_infs = []
            for inf in sample.alleles.filter(novel=True).all():
                if inf != novel:
                    shared = positions[inf.name] & novel_positions
                    if shared:
                        shared_infs.append(inf.name)
            if shared_infs:
                       issues.append((', '.join(shared_infs), sample.name))

        if len(issues):
            report_issue(novel, 'Shared SNP', 'SNP(s) shared with inferences in %d sample(s).<br>Example: %s in sample %s' % (len(issues), issues[0][0], issues[0][1]))


# Report if an allele is present on both chromosomes, or if there is a deletion
def check_for_singleton_infs(novels):
    samples = Samples.objects.all()
    novel_set = frozenset([n.name for n in novels])
    reported_duplicates = {}
    reported_deletions = {}

    for sample in samples:
        for haplo_file in sample.haplotype.all():
            with open(os.path.join(BASE_DIR, 'uploads', haplo_file.file.name), 'r', newline='') as fi:
                reader = csv.reader(fi, dialect='excel-tab')
                header = True
                for row in reader:
                    if header:
                        header = False
                        continue

                    h1 = set([row[2] + '*' + a.lower() for a in row[3].split(',')])
                    h2 = set([row[2] + '*' + a.lower() for a in row[4].split(',')])

                    haplotyped = h1 | h2
                    novel_haplotyped = haplotyped & novel_set
                    if len(novel_haplotyped):
                        deleted = False

                        for hap in haplotyped:
                            if 'del' in hap:
                                deleted = True
                                break

                        if deleted:
                            for n in novel_haplotyped:
                                novel = novels.filter(name=n).first()
                                if novel:
                                    novel_nondels = []
                                    for a in haplotyped:
                                        if 'del' not in a and 'unk' not in a:
                                            novel_nondels.append(a)

                                    if novel.name not in reported_deletions:
                                        reported_deletions[novel.name] = []
                                    if sample.name not in reported_deletions[novel.name]:
                                        report_issue(novel, 'Deletion on other chromosome', 'In sample %s, allele(s) %s present on one chromosome, deletion on other (%s)' % (sample.name, ', '.join(novel_nondels), haplo_file.by_gene), low_confidence=False)
                                        reported_deletions[novel.name].append(sample.name)

                        else:
                            for n in novel_haplotyped:
                                if n in h1 and n in h2:
                                    novel = novels.filter(name=n).first()
                                    if novel:
                                        if novel.name not in reported_duplicates:
                                            reported_duplicates[novel.name] = []
                                        if sample.name not in reported_duplicates[novel.name]:
                                            report_issue(novel, 'Inferred allele on both chromosomes', 'In sample %s, allele %s is present on both chromosomes (%s).' % (sample.name, n, haplo_file.by_gene), low_confidence=True)
                                            reported_duplicates[novel.name].append(sample.name)


def report_issue(novel, issue_category, issue_notes, low_confidence=True):
    issue = AlleleConfidenceReport(allele=novel, category=issue_category, notes=issue_notes)
    issue.save()
    if low_confidence:
        novel.low_confidence = True
    novel.save()
    print("%s: %s: %s: %d: %.2f" % (novel.name, issue_category, issue_notes, novel.appears, novel.max_kdiff))


# find the 1-based index of a nucleotide in a gapped  sequence, given its index in the ungapped sequence
def find_gapped_index(ind_ungapped, gapped_seq):
    ind = 0
    found_nt = 0

    while found_nt < ind_ungapped:
        if gapped_seq[ind] != '.':
            found_nt += 1
        ind += 1

    return ind + 1


# find the family (subgroup) given the name. Assumes IGxxff- type format, where ff is the family
def find_family(gene):
    if '-' not in gene:
        return None

    return gene.split('-')[0][4:]


def get_ref_vh_codon_usage():
    refs = Alleles.objects.exclude(name__regex='^.*(_|Del).*$')
    usage = {}

    for ref in refs:
        if ref.name[3] == 'V':
            family = find_family(ref.name)
            if family not in usage:
                usage[family] = [[],]

            try:
                aa_seq = Seq(ref.seq.upper()).translate(gap='.')

                for i in range(0, len(aa_seq)):
                    if len(usage[family]) <= i+1:
                        usage[family].append([])
                    if aa_seq[i] not in usage[family][i+1]:
                        usage[family][i+1].append(aa_seq[i])
            except TranslationError:
                print('Error translating sequence %s: %s' % (ref.name, sys.exc_info()[1]))

    return usage

