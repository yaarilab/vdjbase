#!/usr/bin/python
import pandas as pd
import sqlite3
import os
import sys
import subprocess
try:
    from website.settings import BASE_DIR, SCRATCH_ROOT, DATABASES
except ImportError:
    from standalone_settings import BASE_DIR, SCRATCH_ROOT, DATABASES

from scripts.graphs import run_rscript


# tables folders
GENOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "tables/merge_genotypes")
HAPLOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "tables/merge_haplotypes")
# graphs_folders
LIST_GENOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/list_genotypes")
HEATMAP_GENOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/heatmap_genotypes")
HEATMAP_HAPLOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/haplotypes")
HTML_GENOTYPES_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/html_genotypes")
PERSONAL_GRAPHS_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/personal_genotypes")
PERSONAL_HAPLO_GRAPHS_FOLDER = os.path.join(SCRATCH_ROOT, "graphs/personal_haplotypes")
# scripts locations
MULTIPLE_GENOTYPE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/html_multiple_genotype_hoverText.R")
HEATMAP_GENOTYPE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/genotype_heatmap.R")
HEATMAP_HAPLOTYPE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/haplotype_heatmap.R")
PERSONAL_GENOTYPE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/Create_html_genotype_personal.R")
PERSONAL_HAPLOTYPE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/Haplotype_plot.R")

SYSDATA = os.path.join(BASE_DIR, "Rscripts/sysdata.rda")


def merge_genotypes(samples, file_name, genes, type_html=True, pseudo_genes=False):
    if not os.path.exists(GENOTYPES_FOLDER):
        os.makedirs(GENOTYPES_FOLDER)

    if not os.path.exists(LIST_GENOTYPES_FOLDER):
        os.makedirs(LIST_GENOTYPES_FOLDER)

    if not os.path.exists(HTML_GENOTYPES_FOLDER):
        os.makedirs(HTML_GENOTYPES_FOLDER)

    genotypes_list = []
    for sample in samples:
        name = sample[0]
        filename = os.path.join(BASE_DIR,'uploads',sample[1])
        genotype = pd.read_csv(filename, sep='\t', dtype=str)
        genotype = genotype[genotype.GENE.isin(genes)]
        genotype.insert(0, 'SUBJECT', name)
        genotypes_list.append(genotype)

    genotypes = pd.concat(genotypes_list)
    geno_path = os.path.join(GENOTYPES_FOLDER, str(file_name +'-genotypes.tab'))

    genotypes.to_csv(geno_path, sep='\t')

    if type_html:
        file_type = "T"
        graph_path = os.path.join(HTML_GENOTYPES_FOLDER, str(file_name + '.html'))
    else:
        file_type = "F"
        graph_path = os.path.join(LIST_GENOTYPES_FOLDER, str(file_name +'.pdf'))

    if pseudo_genes:
        pseudo_genes = "T"
    else:
        pseudo_genes = "F"

    cmd_line = ["Rscript", MULTIPLE_GENOTYPE_SCRIPT, "-i", geno_path, "-o", graph_path, "-s", SYSDATA, "-t", file_type, "-p", pseudo_genes]

    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


def genotypes_heatmap(samples, file_name, genes, kdiff, type_html=True):
    print('Genotype heatmap requested: samples %s, genes %s, kdiff %f, type %s' % (', '.join([x[0] for x in samples]), ', '.join(genes), kdiff, 'html' if type_html else 'pdf'))
    if not os.path.exists(GENOTYPES_FOLDER):
        os.makedirs(GENOTYPES_FOLDER)

    if not os.path.exists(HEATMAP_GENOTYPES_FOLDER):
        os.makedirs(HEATMAP_GENOTYPES_FOLDER)

    if not os.path.exists(HTML_GENOTYPES_FOLDER):
        os.makedirs(HTML_GENOTYPES_FOLDER)

    genotypes = pd.DataFrame()

    for sample in samples:
        name = sample[0]
        filename = os.path.join(BASE_DIR,'uploads',sample[1])
        genotype = pd.read_csv(filename, sep='\t', dtype=str)
        genotype = genotype[genotype.GENE.isin(genes)]
        genotype.insert(0, 'SUBJECT', name)
        genotypes = genotypes.append(genotype)[genotype.columns.tolist()]

    geno_path = os.path.join(GENOTYPES_FOLDER, str(file_name +'-genotypes.tab'))

    genotypes.to_csv(geno_path, sep='\t')
    if type_html:
        file_type = "T"
        graph_path = os.path.join(HTML_GENOTYPES_FOLDER, str(file_name + '.html'))
    else:
        file_type = "F"
        graph_path = os.path.join(HEATMAP_GENOTYPES_FOLDER, str(file_name +'.pdf'))

    if not kdiff:
        kdiff=0

    cmd_line = ["Rscript", HEATMAP_GENOTYPE_SCRIPT,"-i", geno_path, "-o", graph_path, "-s", SYSDATA, "-t", file_type, "-k", str(kdiff)]

    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


def haplotypes_heatmap(samples, file_name, genes, kdiff, type_html=True):
    if not os.path.exists(HAPLOTYPES_FOLDER):
        os.makedirs(HAPLOTYPES_FOLDER)

    if not os.path.exists(HEATMAP_HAPLOTYPES_FOLDER ):
        os.makedirs(HEATMAP_HAPLOTYPES_FOLDER)

    haplotypes = pd.DataFrame()
    for sample in samples:
        name = sample[0]
        filename = os.path.join(BASE_DIR,'uploads',sample[1])
        haplotype = pd.read_csv(filename, sep='\t', dtype=str)
        haplotype = haplotype[haplotype.GENE.isin(genes)]
        haplotype['SUBJECT'] = name
        haplotypes = pd.concat([haplotypes, haplotype], keys=None, ignore_index=True)[haplotype.columns.tolist()]

    haplo_path = os.path.join(HAPLOTYPES_FOLDER , str(file_name +'-haplotypes.tab'))

    haplotypes.to_csv(haplo_path, sep='\t', index=False)
    graph_path = os.path.join(HEATMAP_HAPLOTYPES_FOLDER, str(file_name + '.pdf'))
    # if type_html:
    #     file_type = "T"
    #     graph_path = os.path.join(HTML_GENOTYPES_FOLDER, str(file_name + '.html'))
    # else:
    #     file_type = "F"
    #     graph_path = os.path.join(MULTIPLE_GENOTYPES_FOLDER, str(file_name +'.pdf'))

    if not kdiff:
        kdiff=0

    cmd_line = ["Rscript", HEATMAP_HAPLOTYPE_SCRIPT, "-i", haplo_path, "-o", graph_path, "-s", SYSDATA, "-k", str(kdiff)]

    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


def personal_genotype(sample_name, genotype_file, type_html=True):
    if not os.path.exists(PERSONAL_GRAPHS_FOLDER):
        os.makedirs(PERSONAL_GRAPHS_FOLDER)

    if type_html:
        file_type = "T"
        graph_path = os.path.join(PERSONAL_GRAPHS_FOLDER, str(sample_name + ".html"))
    else:
        file_type = "F"
        graph_path = os.path.join(PERSONAL_GRAPHS_FOLDER, str(sample_name + ".pdf"))

    cmd_line = ["Rscript", MULTIPLE_GENOTYPE_SCRIPT, "-i", genotype_file, "-o", graph_path, "-s", SYSDATA,
                             "-t", file_type, "--samp", sample_name]
    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


def personal_haplotype(sample_name, haplo_name, haplotype_file, type_html=True):
    if not os.path.exists(PERSONAL_HAPLO_GRAPHS_FOLDER):
        os.makedirs(PERSONAL_HAPLO_GRAPHS_FOLDER)

    if type_html:
        file_type = "T"
        graph_path = os.path.join(PERSONAL_HAPLO_GRAPHS_FOLDER, str(haplo_name + ".html"))
    else:
        file_type = "F"
        graph_path = os.path.join(PERSONAL_HAPLO_GRAPHS_FOLDER, str(haplo_name + ".pdf"))

    cmd_line = ["Rscript", PERSONAL_HAPLOTYPE_SCRIPT, "-i", haplotype_file, "-o", graph_path, "-s", SYSDATA,
                             "-t", file_type, "--samp", sample_name]

    if run_rscript(cmd_line, cwd=SCRATCH_ROOT):
        return graph_path
    else:
        return False


def main(argv):
    conn = sqlite3.connect(DATABASES['default']['NAME'])
    c = conn.cursor()
    c.execute("SELECT name, genotype FROM database_samples")
    samples = c.fetchall()

    conn.commit()
    c.close()

    genotypes_list = []

    for sample in samples:
        name = sample[0]
        filename = os.path.join(BASE_DIR,'uploads',sample[1])
        genotype = pd.read_csv(filename, sep='\t')
        genotype.insert(0, 'SUBJECT', name)
        genotypes_list.append(genotype)

    genotypes = pd.concat(genotypes_list)
    genotypes.to_csv('/home/aviv/genotypes.tab', sep='\t')


if __name__ == "__main__":
    main(sys.argv[1:])
