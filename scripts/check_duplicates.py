import sqlite3
import os

def parse():
    # Establish a sqlite3 connection
    conn = sqlite3.connect(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) ,"db.sqlite3"))
    cursor = conn.cursor()


    cursor.execute("SELECT * FROM database_alleles")
    all_alleles = cursor.fetchall()
    count = 0
    duplicate = []
    for allele in all_alleles:
        cursor.execute("SELECT name FROM database_alleles WHERE seq = ?", (allele[2],))
        g = cursor.fetchall()
        if len(g) > 1:
            if allele[1] not in duplicate:
                for a in g:
                    if allele[1] != a[0]:
                        print(allele[1], a[0])
                    duplicate.append(a[0])
    print(len(duplicate))
    cursor.close()
    # Commit the transaction
    conn.commit()

    # Close the database connection
    conn.close()

if __name__ == '__main__':
    parse()