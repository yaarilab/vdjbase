import xlwt
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def dna_samples_export(samples):
    wb = xlwt.Workbook()
    ws0 = wb.add_sheet('dna samples')

    ws0.write(0,0,"Name")
    ws0.write(0,1,"Sequence")
    ws0.write(0,2,"Species")
    ws0.write(0,3,"Tissue")
    ws0.write(0,4,"Protocol")
    ws0.write(0,5,"Amount DNA (ng)")
    ws0.write(0,6,"Number of Cells")
    ws0.write(0,7,"Index 1")
    ws0.write(0,8,"Index 2")
    ws0.write(0,9,"Primer")
    ws0.write(0,10,"Number of Cycles PCR")
    ws0.write(0,11,"BOX")
    ws0.write(0,12,"Location")

    i = 1
    for s in samples:
        print("l")
    wb.save(BASE_DIR + "/tables/" + "bla.xlsx")

if __name__ == "__main__":
    dna_samples_export("dsfds")


