# Path settings to use when scripts are called standalone rather than as part of the website

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SCRATCH_ROOT = os.path.join(BASE_DIR, 'scratch/')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
