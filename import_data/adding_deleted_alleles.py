#!/usr/bin/python
import sqlite3
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
c = conn.cursor()

c.execute("SELECT id, name FROM database_gene")
genes = c.fetchall()

insert_allele_query = "INSERT INTO database_alleles (name, seq, seq_len, gene_id, is_single_allele, appears) " \
                      "VALUES (?, ?, ?, ?, ?, 0)"

for gene in genes:
    gene_id = gene[0]
    gene_name = gene[1]

    allele_name = gene_name + "*Del"
    seq = None
    seq_len = 0

    values = (allele_name, seq, seq_len, gene_id, False)
    c.execute(insert_allele_query, values)

conn.commit()
conn.close()