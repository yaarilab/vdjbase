# Generated by Django 3.0.3 on 2020-03-11 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0029_haplotypeevidence_hap_gene'),
    ]

    operations = [
        migrations.AddField(
            model_name='samples',
            name='novel_inferences',
            field=models.IntegerField(default=0),
        ),
    ]
