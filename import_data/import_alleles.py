import sqlite3
import os

def parse(fasta):
    # Establish a sqlite3 connection
    conn = sqlite3.connect(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) ,"db.sqlite3"))
    cursor = conn.cursor()
    insert_allele_query = "INSERT INTO database_alleles (name, seq, seq_len, gene_id, is_single_allele, appears) VALUES (?, ?, ?, ?, ?, 0)"
    insert_gene_query = "INSERT INTO database_gene (name, type, family, species) VALUES (?, ?, ?, ?)"

    header = None
    with open(fasta, 'r') as fin:
        allele = ''

        for line in fin:
            if line.startswith('>'):
                if header is not None:
                    try:
                        seq = "".join(seq.split())
                        cursor.execute("SELECT id, similar, name FROM database_alleles WHERE seq = ?", (seq,))
                        a = cursor.fetchall()[0]
                        if a[0] is not None:
                            if sorted(a[2]) != sorted(allele):
                                note = ''
                                allele = "|" + allele + "|"
                                if a[1] is None:
                                    note += allele
                                    cursor.execute("UPDATE database_alleles SET similar = ? WHERE id = ?", (note, a[0],))
                                    print("exist seq " + allele)
                                else:
                                    note = a[1]
                                    if allele not in note:
                                        note +=", " + allele
                                        cursor.execute("UPDATE database_alleles SET similar = ? WHERE id = ?", (note, a[0],))
                                        print("exist seq " + allele)
                        else:
                            cursor.execute("SELECT id FROM database_alleles WHERE name  = ?", (allele,))
                            values = (allele, seq, len(seq), gid, True)
                            cursor.execute(insert_allele_query, values)
                            print(allele)
                    except:
                        cursor.execute("SELECT id FROM database_alleles WHERE name  = ?", (allele,))
                        values = (allele, seq, len(seq), gid, True)
                        cursor.execute(insert_allele_query, values)
                        print("check " + allele)

                seq = ''
                allele = line.split('|', line.count('|'))[1]
                gene = allele.split('*', 1)[0]
                family = gene.split('-', 1)[0]

                cursor.execute("SELECT id FROM database_gene WHERE name  = ?", (gene,))
                gid = cursor.fetchall()
                if gid == []:
                    values = (gene, gene[:4], family, "human")
                    cursor.execute(insert_gene_query, values)
                    gid = cursor.lastrowid
                    conn.commit()

                else:
                    gid = gid[0][0]

                print(gid)
                header = line
            else:
                seq += line
        try:
            seq = "".join(seq.split())
            cursor.execute("SELECT id, similar, name FROM database_alleles WHERE seq = ?", (seq,))
            a = cursor.fetchall()[0]
            if a[0] is not None:
                if sorted(a[2]) != sorted(allele):
                    note = ''
                    allele = "|" + allele + "|"
                    if a[1] is None:
                        note += allele
                        cursor.execute("UPDATE database_alleles SET similar = ? WHERE id = ?", (note, a[0],))
                        print("exist seq " + allele)
                    else:
                        note = a[1]
                        if allele not in note:
                            note += ", " + allele
                            cursor.execute("UPDATE database_alleles SET similar = ? WHERE id = ?", (note, a[0],))
                            print("exist seq " + allele)
            else:
                cursor.execute("SELECT id FROM database_alleles WHERE name  = ?", (allele,))
                values = (allele, seq, len(seq), gid, True)
                cursor.execute(insert_allele_query, values)
                print(allele)

        except:
            cursor.execute("SELECT id FROM database_alleles WHERE name  = ?", (allele,))
            values = (allele, seq, len(seq), gid, True)
            cursor.execute(insert_allele_query, values)
            print("check " + allele)
    cursor.close()
    # Commit the transaction
    conn.commit()

    # Close the database connection
    conn.close()

if __name__ == '__main__':
    for family in ["IGHV", "IGHD", "IGHJ"]:
        parse("/home/aviv/IMGT/HUMAN/VDJ/" + family + ".fasta")


