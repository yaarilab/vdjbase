# Generated by Django 3.0.3 on 2020-03-11 17:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0030_samples_novel_inferences'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='samples',
            name='novel_inferences',
        ),
    ]
