"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from website import settings
from . import views
app_name = 'database'

urlpatterns = [
    url(r'^$', views.index, name='home'),

    # tables
    url(r'^data/Samples$', views.sample_table.as_view(), name='sample-data'),
    url(r'^data/Alleles$', views.alleles_table.as_view(), name='allele-data'),

    url(r'^Graph/', views.presenting_graph.as_view(), name='graph-present'),

    # genotype urls handler
    url(r'^genotype/(?P<sample_name>[\w\-]+)$', views.down_geno, name='down-geno'),
    url(r'^genotype_graph/(?P<sample_name>[\w\-]+)/(?P<html>[0-1])$', views.genotype_graph, name='geno-graph'),
    url(r'^genotype_stats/(?P<sample_name>[\w\-]+)$', views.down_geno_stats, name='down-stats'),

    # hapotype urls handler
    url(r'^haplotype/(?P<id>[\w\-]+)$', views.down_haplotype, name='down-haplotype'),
    url(r'^haplotype_graph/(?P<id>[\w\-]+)/(?P<html>[0-1])$', views.haplotype_graph, name='haplotype-graph'),
    url(r'^User_Guide/', views.guide.as_view(), name='user-guide'),

    url(r'^Explore_Data$', views.explore.as_view(), name='explore-data'),

    url(r'^Licensing$', views.licensing.as_view(), name='licensing'),
]


#user handler add to url patterns in case of adding users
"""
    url(r'^register$', views.UserFormView.as_view(), name='register'),
    url(r'^login$', views.login.as_view(), name='login'),
    url(r'^logout$', views.logout, name='logout'),"""