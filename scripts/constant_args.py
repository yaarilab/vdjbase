import os

try:
    from website.settings import SCRATCH_ROOT
except ImportError:
    from standalone_settings import SCRATCH_ROOT

# paths
GRAPH_DIR = os.path.join(SCRATCH_ROOT, 'graphs')

# lists
IGH_ORDER = ['IGHV3-74', 'IGHV3-73', 'IGHV3-72', 'IGHV2-70', 'IGHV1-69-2', 'IGHV2-70D', 'IGHV1-69', 'IGHV3-66',
             'IGHV3-64', 'IGHV4-61', 'IGHV4-59', 'IGHV1-58', 'IGHV3-53', 'IGHV5-51', 'IGHV3-49', 'IGHV3-48', 'IGHV1-46',
             'IGHV1-45', 'IGHV3-43D', 'IGHV3-43', 'IGHV4-39', 'IGHV4-38-2', 'IGHV3-38', 'IGHV4-34', 'IGHV3-33',
             'IGHV4-31', 'IGHV4-30-4', 'IGHV4-30-2', 'IGHV3-30-3', 'IGHV3-30', 'IGHV4-28', 'IGHV2-26', 'IGHV1-24',
             'IGHV3-23', 'IGHV3-21', 'IGHV3-20', 'IGHV1-18', 'IGHV3-15', 'IGHV3-13', 'IGHV3-11', 'IGHV3-9', 'IGHV5-10-1',
             'IGHV1-8', 'IGHV3-64D', 'IGHV3-7', 'IGHV2-5', 'IGHV7-4-1', 'IGHV4-4', 'IGHV1-3', 'IGHV1-2', 'IGHV6-1',
             'IGHD1-1', 'IGHD2-2', 'IGHD3-3', 'IGHD6-6', 'IGHD1-7', 'IGHD2-8', 'IGHD3-9', 'IGHD3-10', 'IGHD4-11',
             'IGHD5-12', 'IGHD6-13', 'IGHD2-15', 'IGHD3-16', 'IGHD4-17', 'IGHD5-18', 'IGHD6-19', 'IGHD1-20', 'IGHD2-21',
             'IGHD3-22', 'IGHD4-23', 'IGHD5-24', 'IGHD6-25', 'IGHD1-26', 'IGHD7-27', 'IGHJ1', 'IGHJ2', 'IGHJ3', 'IGHJ4',
             'IGHJ5', 'IGHJ6']

IGHV_ORDER = ['3-74', '3-73', '3-72', '2-70', '1-69-2', '2-70D', '1-69', '3-66', '3-64', '4-61', '4-59', '1-58', '3-53',
              '5-51', '3-49', '3-48', '1-46', '1-45', '3-43D', '3-43', '4-39', '4-38-2', '3-38', '4-34', '3-33', '4-31',
              '4-30-4', '4-30-2', '3-30-3', '3-30', '4-28', '2-26', '1-24', '3-23', '3-21', '3-20', '1-18', '3-15',
              '3-13', '3-11', '3-9', '5-10-1', '1-8', '3-64D', '3-7', '2-5', '7-4-1', '4-4', '1-3', '1-2', '6-1']

IGHD_ORDER = ['1-1', '2-2', '3-3', '6-6', '1-7', '2-8', '3-9', '3-10', '4-11', '5-12', '6-13', '2-15', '3-16', '4-17',
              '5-18', '6-19', '1-20', '2-21', '3-22', '4-23', '5-24', '6-25', '1-26', '7-27']

IGHJ_ORDER = ['1', '2', '3', '4', '5', '6']

