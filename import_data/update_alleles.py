import sqlite3, os, re
import pandas as pd

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
GENE_COLUMN = "GENE"
ALLELES_COLUMN = "ALLELES"
GENOTYPED_ALLELES_COLUMN = "GENOTYPED_ALLELES"


def change_allele_names_by_nameslist(alleles_names_list):
    """
    The function gets list of allele couples and replace the first allele name in the couple to the second one.
    :param alleles_names_list: list of allele couples
    """
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()
    query_set_allele_name = "UPDATE database_alleles SET name = ? WHERE name  = ?"
    query_get_samplesList_by_alleleID = "SELECT sample_id " \
                                        "FROM database_alleles_samples, database_alleles AS allele " \
                                        "WHERE allele.name  = ? AND allele.id = allele_id"

    for couple in alleles_names_list:
        gene = couple[0].split("*")[0]
        oldname = couple[0].split("*")[1].upper()
        newname = couple[1].split("*")[1].upper()
        c.execute(query_get_samplesList_by_alleleID, (str(couple[0]),))

        try:
            samples_list = [x[0] for x in c.fetchall()]
            questions_marks = "(" + ','.join(['?']*len(samples_list)) + " )"

            query_get_genotypes = "SELECT genotype FROM database_samples WHERE id in " + questions_marks
            c.execute(query_get_genotypes, (samples_list))
            gentype_files = [x[0] for x in c.fetchall()]

            for genotype_file in gentype_files:
                filename = os.path.join(BASE_DIR,'uploads',genotype_file)
                genotype = pd.read_csv(filename, sep='\t', dtype=str)
                geno_alleles = str(genotype[genotype[GENE_COLUMN] == gene][GENOTYPED_ALLELES_COLUMN].iloc[0])
                geno_alleles = re.sub(oldname+"$", newname, geno_alleles.replace(oldname+",", newname+","))
                alleles = str(genotype[genotype[GENE_COLUMN] == gene][ALLELES_COLUMN].iloc[0])
                alleles = re.sub(oldname+"$", newname, alleles.replace(oldname+",", newname+","))
                genotype.loc[genotype[GENE_COLUMN] == gene, GENOTYPED_ALLELES_COLUMN] = geno_alleles
                genotype.loc[genotype[GENE_COLUMN] == gene, ALLELES_COLUMN] = alleles
                genotype.to_csv(filename, sep='\t', index=False)

            query_get_genotypes = "SELECT file " \
                                  "FROM database_samples_haplotype, database_haplotypes_files AS haplo " \
                                  "WHERE haplotypes_files_id = haplo.id AND samples_id in " + questions_marks
            c.execute(query_get_genotypes, (samples_list))
            haplotype_files = [x[0] for x in c.fetchall()]

            for haplotype_file in haplotype_files :
                filename = os.path.join(BASE_DIR,'uploads',haplotype_file)
                haplotype = pd.read_csv(filename, sep='\t', dtype=str)
                haplotype.loc[haplotype[GENE_COLUMN] == gene, 2] = \
                    re.sub(oldname+"$", newname, str(haplotype[haplotype[GENE_COLUMN] == gene].iloc[0,2]).replace(oldname+",", newname+","))
                haplotype.loc[haplotype[GENE_COLUMN] == gene, 3] = \
                    re.sub(oldname+"$", newname, str(haplotype[haplotype[GENE_COLUMN] == gene].iloc[0,3]).replace(oldname+",", newname+","))
                haplotype.loc[haplotype[GENE_COLUMN] == gene, 4] = \
                    re.sub(oldname+"$", newname, str(haplotype[haplotype[GENE_COLUMN] == gene].iloc[0,4]).replace(oldname+",", newname+","))
                haplotype.to_csv(filename, sep='\t', index=False)

        except:
            print("Couldn't find " + couple[0] + " allele over the database")

        c.execute(query_set_allele_name, (str(couple[1]), str(couple[0]),))

    conn.commit()
    c.close()
    conn.close()

