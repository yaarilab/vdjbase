import pandas as pd
#!/usr/bin/python
import sqlite3
import os, re
import sys, getopt
from itertools import chain

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def check_patterns():
    """
        This function calculate the alleles that meets condition of pattern.
        """
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    cursor = conn.cursor()

    # take all genes
    cursor.execute("SELECT id FROM database_gene WHERE name LIKE '%V%'")
    genes_id = cursor.fetchall()
    query_select_patterns = \
        "SELECT id, seq FROM database_alleles WHERE name NOT LIKE '%Del' AND is_single_allele = ? AND gene_id = ?"
    query_select_alleles = "SELECT id, seq FROM database_alleles WHERE name NOT LIKE '%Del' AND gene_id = ?"
    insertion_allele_pattern = "INSERT INTO database_alleles_patterns (allele_in_p_id, pattern_id) VALUES (?, ?)"
    check_pattern = "SELECT COUNT(*) FROM database_alleles_patterns WHERE allele_in_p_id=? AND pattern_id=?"

    for gene_id in genes_id:
        cursor.execute(query_select_alleles, (gene_id[0],))
        alleles = cursor.fetchall()
        cursor.execute(query_select_patterns , (0, gene_id[0]))
        patterns = cursor.fetchall()

        for pattern in patterns:
            pattern_id = pattern[0]
            pattern_seq = pattern[1].replace("n","[a,g,c,t,n]")

            for allele in alleles:
                allele_id = allele[0]
                allele_seq = allele[1]

                if (pattern_id != allele_id):
                    if (re.match(pattern_seq, allele_seq)):
                        # check if pattern already exists
                        cursor.execute(check_pattern, (allele_id, pattern_id))
                        if not cursor.fetchall()[0][0]:
                            cursor.execute(insertion_allele_pattern, (allele_id, pattern_id))

    conn.commit()
    cursor.close()
    conn.close()

if __name__ == '__main__':
    check_patterns()