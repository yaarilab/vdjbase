import xlwt
import os, subprocess

from website.settings import BASE_DIR, SCRATCH_ROOT
from scripts.graphs import run_rscript


GRAPH_DIR = os.path.join(SCRATCH_ROOT, 'graphs/alleles_distribution')
TABLES_DIR = os.path.join(SCRATCH_ROOT, 'tables', 'alleles_distribution')
RSCRIPT_PATH = os.path.join(BASE_DIR, "Rscripts/allele_appeareance2.R")

def export_sheet(sheet, arr):
    row=1
    for col in arr:
        i = 0
        for field in col:
            try:
                sheet.write(row, i, field)
            except:
                pass
            i+=1
        row+=1


def write_gene(book, gene):
    sheet = book.add_sheet(gene[0].replace("/","-"))

    sheet.write(0,0, "Allele")
    sheet.write(0,1, "Appeared")

    for i in range (0, len(gene[1])):
        sheet.write(i + 1, 0, gene[1][i])
        sheet.write(i + 1, 1, gene[2][i])

    if len(gene) == 4:
        sheet.write(0,2, "Total")
        sheet.write(1,2, gene[3])

def output(filename, genes):
    if not os.path.exists(TABLES_DIR):
        os.makedirs(TABLES_DIR)

    if not os.path.exists(GRAPH_DIR):
        os.makedirs(GRAPH_DIR)

    path = os.path.join(TABLES_DIR, filename + ".xls")
    book = xlwt.Workbook()

    for gene in genes:
        if len(gene[1]) > 0:
            write_gene(book, gene)

    book.save(path)
    p_file = os.path.join(GRAPH_DIR, filename + ".pdf")

    cmd_line = ["Rscript", RSCRIPT_PATH, "-i", path, "-o", p_file]

    if run_rscript(cmd_line):
        return p_file
    else:
        return False

def write_sheet(book, table, titles, name):
    sheet = book.add_sheet(name)
    coulms = len(titles) - 1
    j = 0
    while j <= coulms:
        sheet.write(0,j, titles[j])
        j += 1

    for i in range (0, len(table)):
        j = 0
        while j <= coulms:
            sheet.write(i+1, j, table[i][j])
            j += 1

def geno_table(filename, genotypes):
    if not os.path.exists(TABLES_DIR):
        os.makedirs(TABLES_DIR)
    path = os.path.join(TABLES_DIR, filename + "-geno.xls")
    book = xlwt.Workbook()

    for geno in genotypes:
        write_sheet(book, geno[1], ["Gene", "Allele", "kdiff"], str(geno[0]))

    book.save(path)
    """p_file = os.path.join(GRAPH_DIR, filename + ".pdf")
    os.system("Rscript " + os.path.join(BASE_DIR, "Rscripts/allele_appeareance2.R") + " -i " + path + " -o " + p_file)"""
    return path