
import pandas as pd
from scripts.constant_args import *

import os
import subprocess

try:
    from website.settings import BASE_DIR, SCRATCH_ROOT, DATABASES
except ImportError:
    from standalone_settings import BASE_DIR, SCRATCH_ROOT, DATABASES


# tables folders
GENE_USAGE_TABLES = os.path.join(SCRATCH_ROOT, "tables/gene_usage")
ALLELES_USAGE_TABLES = os.path.join(SCRATCH_ROOT, 'tables/alleles_usage')
HETEROZYGOUS_TABLES = os.path.join(SCRATCH_ROOT, 'tables/heterozygous')
# graphs_folders
GENE_USAGE_GRAPHS = os.path.join(SCRATCH_ROOT, "graphs/gene_usage")
ALLELES_USAGE_GRAPHS = os.path.join(SCRATCH_ROOT, 'graphs/alleles_usage')
HETEROZYGOUS_GRAPHS = os.path.join(SCRATCH_ROOT, 'graphs/heterozygous')
# scripts locations
GENE_USAGE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/Gene_Usage.R")
ALLELES_USAGE_SCRIPT = os.path.join(BASE_DIR, "Rscripts/Alleles_Usage.R")
HETEROZYGOUS_SCRIPT = os.path.join(BASE_DIR, "Rscripts/Heterozygous.R")

SYSDATA = os.path.join(BASE_DIR, "Rscripts/sysdata.rda")

#########################################################################################################
#
#                     generic Rscript runner
#
##########################################################################################################


def run_rscript(cmd_line, cwd=BASE_DIR+'/Rscripts'):
    print("Running Rscript: '%s'\n" % ' '.join(cmd_line))
    proc = subprocess.Popen(cmd_line, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    (stdout, stderr) = proc.communicate()

    print(str(stdout.decode("utf-8")))
    if proc.returncode != 0:
        if "Execution halted" in str(stderr.decode("utf-8")):
            print("Got error in Rscript:\n" + str(stderr.decode("utf-8")))
            return False

    return True


#########################################################################################################
#
#                     hetrozygous interactive graph
#
##########################################################################################################


def heterozygous_graph(gene_hetrozygous_dis, name):
    if not os.path.exists(HETEROZYGOUS_TABLES):
        os.makedirs(HETEROZYGOUS_TABLES)

    if not os.path.exists(HETEROZYGOUS_GRAPHS):
        os.makedirs(HETEROZYGOUS_GRAPHS)

    # convert the list to dataframe.
    labels = ['GENE', 'HM', 'HT']
    df = pd.DataFrame(gene_hetrozygous_dis, columns=labels)

    # saving the dataframe as input file to the R script
    input_path = os.path.join(HETEROZYGOUS_TABLES, str(name + '-heterozygous.tab'))
    df.to_csv(input_path, sep='\t', index=False)

    # the graph's file name (the output file of the R script)
    graph_path = os.path.join(HETEROZYGOUS_GRAPHS, str(name + '-heterozygous.html'))


    cmd_line = ["Rscript", HETEROZYGOUS_SCRIPT, "-i", input_path, "-o", graph_path, "-s", SYSDATA]
    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


#########################################################################################################
#
#     SHOW HOW MANY ALLELES OF EACH GENE HAVE APPEARED
#
##########################################################################################################


def alleles_usage_graph(genes_alleles, name):
    if not os.path.exists(ALLELES_USAGE_TABLES):
        os.makedirs(ALLELES_USAGE_TABLES)

    if not os.path.exists(ALLELES_USAGE_GRAPHS):
        os.makedirs(ALLELES_USAGE_GRAPHS)

    # convert the list to dataframe.
    labels = ['GENE', 'COUNT']
    df = pd.DataFrame(genes_alleles, columns=labels)

    # saving the dataframe as input file to the R script
    input_path = os.path.join(ALLELES_USAGE_TABLES, str(name + '-allele_usage.tab'))
    df.to_csv(input_path, sep='\t', index=False)

    # the graph's file name (the output file of the R script)
    graph_path = os.path.join(ALLELES_USAGE_GRAPHS, str(name + '-allele_usage.html'))
    cmd_line = ["Rscript", ALLELES_USAGE_SCRIPT, "-i", input_path, "-o", graph_path, "-s", SYSDATA]

    if run_rscript(cmd_line):
        return graph_path
    else:
        return False


#########################################################################################################
#
#     frequencies distribution graph
#
##########################################################################################################

def freq_dist(genes_frequencies, name, type_html=False):
    if not os.path.exists(GENE_USAGE_TABLES):
        os.makedirs(GENE_USAGE_TABLES)

    if not os.path.exists(GENE_USAGE_GRAPHS):
        os.makedirs(GENE_USAGE_GRAPHS)

    labels = ['GENE', 'FREQ']

    genes_frequencies_df = pd.DataFrame(columns=labels)
    for gene, usages in genes_frequencies.items():
        genes_frequencies_df = genes_frequencies_df.append({'GENE': gene, 'FREQ':",".join([str(x) for x in usages])}, ignore_index=True)

    input_path = os.path.join(GENE_USAGE_TABLES, str(name + '-gene_usage.tab'))
    genes_frequencies_df.to_csv(input_path, sep='\t', index=False)

    if type_html:
        file_type = "T"
        graph_path = os.path.join(GENE_USAGE_GRAPHS, str(name + '-gene_usage' + '.html'))
    else:
        file_type = "F"
        graph_path = os.path.join(GENE_USAGE_GRAPHS, str(name + '-gene_usage' + '.pdf'))

    cmd_line = ["Rscript", GENE_USAGE_SCRIPT, "-i", input_path, "-o", graph_path, "-s", SYSDATA, "-t", file_type]

    if run_rscript(cmd_line):
        return graph_path
    else:
        return False
