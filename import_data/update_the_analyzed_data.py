import pandas as pd
#!/usr/bin/python
import sqlite3
import os, math
import sys, getopt
from itertools import chain

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

FREQ_BY_SEQ = "Freq_by_Seq"
FREQ_BY_CLONE = "Freq_by_Clone"


def update_alleles_appearance():
    """
    This function update the appearance of the alleles according to the number of patients.
    """
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT id FROM database_alleles")
    alleles = c.fetchall()
    for a in alleles:
        aid = a[0]
        c.execute("SELECT patient_id FROM database_alleles_samples WHERE hap = ? AND allele_id = ?", ("geno", aid))
        appears = len(set(c.fetchall()))
        c.execute("UPDATE database_alleles SET appears = ? WHERE id = ?", (appears, aid,))

    conn.commit()
    c.close()


def calculate_genes_frequencies():
    """
    upload the genes frequencies of the new samplesby calculating them from the genotype file.
    """

    query_gene_id = "SELECT id FROM database_gene WHERE name = ?"
    query_insert_freq = "INSERT INTO database_genes_distribution (gene_id, patient_id, sample_id, " \
                        "frequency, count_by_clones) VALUES (?, ?, ?, ?, ?)"
    query_dup_freq = "SELECT id FROM database_genes_distribution WHERE gene_id = ? AND patient_id = ? AND " \
                     "sample_id = ? AND frequency = ? AND count_by_clones = ?"

    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT name, id FROM database_gene WHERE name NOT LIKE '%OR%' AND name NOT LIKE '%NL%'")
    all_genes = c.fetchall()
    all_genes_names = [str(x[0]) for x in all_genes]
    map_genes = {x:int(y[1]) for (x,y) in zip(all_genes_names, all_genes)}
    try:
        c.execute("SELECT sample_id FROM database_genes_distribution")
        samples = list(chain.from_iterable(c.fetchall()))
        c.execute("SELECT id, patient_id, genotype FROM database_samples WHERE id NOT IN (%s)" %','.join('?'*len(samples)), samples)
    except:
        c.execute("SELECT id, patient_id, genotype FROM database_samples")
    samples = c.fetchall()

    for sample in samples:
        sample_id = sample[0]
        patient_id = sample[1]
        filename = os.path.join(BASE_DIR,'uploads',sample[2])
        genotype = pd.read_csv(filename, sep='\t')

        frequencies_by_clone = {}
        frequencies_by_seq = {}

        # counting the appearance of the genes and the total of each family
        family_total_seq = {}
        family_total_clone = {}
        for gene, count_seq, count_clone in zip(genotype["GENE"], genotype[FREQ_BY_SEQ], genotype[FREQ_BY_CLONE]):
            family = gene[:4]
            if family not in family_total_seq.keys():
                family_total_seq[family] = 0
                family_total_clone[family] = 0

            if isinstance(count_seq, float):
                if math.isnan(count_seq):
                    count_seq = 0
                count_seq = str(count_seq)

            if isinstance(count_clone, float):
                if math.isnan(count_clone):
                    count_clone = 0
                count_clone = str(count_clone)

            frequencies_by_seq[gene] = 0
            for x in count_seq.split(","):
                frequencies_by_seq[gene] += int(x)
            family_total_seq[family] += frequencies_by_seq[gene]

            frequencies_by_clone[gene] = 0
            for x in count_clone.split(","):
                frequencies_by_clone[gene] += int(x)
            family_total_clone[family] += frequencies_by_clone[gene]

        # calculate the frequency of each gene acording to the family
        for gene in frequencies_by_seq.keys():
            family = gene[:4]
            frequencies_by_seq[gene] /= float(family_total_seq[family])
            frequencies_by_clone[gene] /= float(family_total_clone[family])

        for gene in frequencies_by_seq.keys():
            c.execute(query_gene_id, (gene,))
            gene_id = c.fetchall()[0][0]

            values_clone = (gene_id, patient_id, sample_id, frequencies_by_clone[gene], True)
            values_seq = (gene_id, patient_id, sample_id, frequencies_by_seq[gene], False)

            c.execute(query_dup_freq, values_clone)
            if c.fetchall() == []:
                c.execute(query_insert_freq, values_clone)

            c.execute(query_dup_freq, values_seq)
            if c.fetchall() == []:
                c.execute(query_insert_freq, values_seq)

    conn.commit()
    c.close()

