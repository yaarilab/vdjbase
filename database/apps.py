# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig
from subprocess import check_output

commit_id = b''

class dbConfig(AppConfig):
    global commit_id
    name = 'database'

    try:
        commit_id = check_output("git rev-parse --short HEAD")
        commit_id = commit_id.decode("utf-8").strip()
    except:
        commit_id = 'NA'