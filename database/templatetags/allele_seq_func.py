from django import template

register = template.Library()

@register.simple_tag(name="show_seq")
def show_seq(seq):
    max_in_row = 50
    new_seq = ""
    i = 0
    while i < len(seq):
        new_seq += seq[i:min(i+max_in_row, len(seq))] + "\n"
        i += max_in_row
    return new_seq


@register.simple_tag(name="show_similar")
def show_similar(similar):
    return similar.replace("|", "")