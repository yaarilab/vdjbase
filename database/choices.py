
WITH_PSEUDO_CHOICES = [
    (0, "Exclude"),
    (1, "Include")
]

SPECIE_CHOICES = (
    ("", ""),
    ("human", "Human"),
    ("mouse", "Mouse"),
    ("rat", "Rat"),
    ("rabbit", "Rabbit"),
    ("rhesus", "Rhesus Monkey"),
)

TISSUE_CHOICES = (
    ("", ""),
    ("blood", "Blood"),
    ("spleen", "Spleen"),
    ("cancer", "Cancer"),
    ("lymphatic_system", "Lymphatic system"),
    ("thymus", "Thymus"),
    ("gut_normal", "Gut normal"),
    ("gut_cancer", "Gut cancer"),
    ("ddw", "DDW"),
)

TYPE_GENE_CHOICES = (
    ("", ""),
    ("IGHV", "IGHV"),
    ("IGHD", "IGHD"),
    ("IGHJ", "IGHJ"),
    ("IGLV", "IGLV"),
    ("IGLJ", "IGLJ"),
    ("IGKJ", "IGKJ"),
    ("IGKV", "IGKV"),

)

SEX_CHOICES =(
    ("", ""),
    ("m", "Male"),
    ("f", "Female"),
)

HELIX_CHOICES =(
    ("", ""),
    ("rna", "RNA"),
    ("dna", "DNA"),
)

IG_GROUP_CHOICES =(
    ("", ""),
    ("heavy", "Heavy"),
    ("light", "Light"),
)

NUM_ROWS_CHOICES =(
    (10, 10),
    (15, 15),
    (20, 20),
    (25, 25),
)

CLINICAL_STATUS =(
    ("", ""),
    ("healthy", "Healthy"),
    ("sick", "SICK"),
)

GENO_DETECTION = (
    ("geno", "Geno"),
    ("tigger", "Tigger"),
    ("igdiscover", "IgDiscover"),
)

