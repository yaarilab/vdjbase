# Generated by Django 3.0.3 on 2020-03-06 13:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0027_alleles_max_kdiff'),
    ]

    operations = [
        migrations.CreateModel(
            name='HaplotypeEvidence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('allele', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.Alleles')),
                ('sample', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.Samples')),
            ],
        ),
    ]
