# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse
from django.db import models
from .choices import *


class Seq_Protocols(models.Model):
    name = models.CharField(max_length=250, unique=True)
    umi = models.BooleanField()
    helix = models.CharField(max_length=8, choices=HELIX_CHOICES)
    sequencing_platform = models.CharField(max_length=40, default="MiSeq 2x300", null=True)
    sequencing_length = models.CharField(max_length=20, null=True)
    primers_3_location = models.CharField(max_length=50, null=True)
    primers_5_location = models.CharField(max_length=50, null=True)

    def __unicode__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('database:protocol-add')

    def __str__(self):
        return str(self.name)


class Tissue_Pro(models.Model):
    name = models.CharField(max_length=250, unique=True)
    species = models.CharField(max_length=20, choices=SPECIE_CHOICES)
    tissue = models.CharField(max_length=100, choices=TISSUE_CHOICES)
    cell_type = models.CharField(max_length=30)
    sub_cell_type = models.CharField(max_length=30)
    isotype = models.CharField(max_length= 30)

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)


class Studies(models.Model):
    name = models.CharField(max_length=50, unique=True)
    institute = models.CharField(max_length=50)
    researcher = models.CharField(max_length=50)
    num_subjects = models.IntegerField()
    num_samples = models.IntegerField()
    reference = models.URLField(null=True, default=None)
    contact = models.URLField(null=True, default=None)
    accession_id = models.CharField(max_length=50, null=True)
    accession_reference = models.URLField(null=True, default=None)

    def __str__(self):
        return str(self.name)


class Gene(models.Model):
    name = models.CharField(max_length=250, unique=True)
    type = models.CharField(max_length=20, choices=TYPE_GENE_CHOICES)
    family = models.CharField(max_length=20)
    species = models.CharField(max_length=20, choices=SPECIE_CHOICES)

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)



class Alleles(models.Model):
    name = models.CharField(max_length=30, unique=True)
    gene = models.ForeignKey(Gene, on_delete=models.DO_NOTHING)
    seq = models.TextField(max_length=500, unique=True, verbose_name='Sequence', null=True)
    seq_len = models.CharField(max_length=50, verbose_name='Sequence length')
    similar = models.CharField(max_length=250, null=True, default=None)
    appears = models.IntegerField(default=0)
    max_kdiff = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    is_single_allele = models.BooleanField(default=True)
    novel = models.BooleanField(default=False)
    low_confidence = models.BooleanField(default=False)
    closest_ref = models.ForeignKey('self', unique=False, related_name='novel_variants', on_delete=models.DO_NOTHING, default=None, null=True)
    samples = models.ManyToManyField('Samples', through='Alleles_Samples', through_fields=('allele', 'sample'), related_name='+')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def get_confidence_count(self):
        return self.alleleconfidencereport_set.count()

    def get_confidence_reports(self):
        return self.alleleconfidencereport_set.all()


class SNPs(models.Model):
    pos = models.IntegerField(default=0)
    from_base = models.CharField(max_length=1)
    to_base = models.CharField(max_length=1)
    allele = models.ForeignKey(Alleles, on_delete=models.CASCADE)


class Patients(models.Model):
    name = models.CharField(max_length=50, unique=True)
    name_in_paper = models.CharField(max_length=30, unique=False, null=True, default=None)
    sex = models.CharField(max_length=20, choices=SEX_CHOICES)
    ethnic = models.CharField(max_length= 50)
    country = models.CharField(max_length= 50, null=True, default=None, verbose_name='Country of origin')
    study = models.ForeignKey(Studies, on_delete=models.CASCADE)
    status = models.CharField(max_length=100, choices=CLINICAL_STATUS)
    cohort = models.CharField(max_length=50, null=True)
    age = models.IntegerField(null=True)
    alleles = models.ManyToManyField('Alleles', through='Alleles_Samples', through_fields=('patient', 'allele'))

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)


class Geno_Detection(models.Model):
    name = models.CharField(max_length=250, unique=True)
    detection = models.CharField(max_length=20, choices=GENO_DETECTION)
    prepro_tool = models.CharField(max_length=50)
    aligner_tool = models.CharField(max_length=50)
    aligner_ver = models.CharField(max_length=20)
    aligner_reference = models.CharField(max_length=50, null=True)
    geno_tool = models.CharField(max_length=50)
    geno_ver = models.CharField(max_length=20)
    haplotype_tool = models.CharField(max_length=50, null=True)
    haplotype_ver = models.CharField(max_length=20, null=True)
    single_assignment = models.BooleanField(default=False)

    def __str__(self):
        return str(self.detection)


class Haplotypes_files(models.Model):
    by_gene = models.CharField(max_length=50)
    allele_col1 = models.CharField(max_length=50)
    allele_col2 = models.CharField(max_length=50)
    file = models.FileField(upload_to='samples/', null=True, default=None)


class Samples(models.Model):
    name = models.CharField(max_length=250, unique=True)
    study = models.ForeignKey(Studies, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patients, on_delete=models.DO_NOTHING)
    seq_protocol = models.ForeignKey(Seq_Protocols, on_delete=models.DO_NOTHING)
    tissue_pro = models.ForeignKey(Tissue_Pro, on_delete=models.DO_NOTHING)
    chain = models.CharField(max_length=30)
    geno_detection = models.ForeignKey(Geno_Detection, on_delete=models.DO_NOTHING)
    row_reads = models.IntegerField()
    haplotype = models.ManyToManyField(Haplotypes_files)
    genotype = models.FileField(upload_to='samples/', null=True, default=None)
    genotype_stats = models.FileField(upload_to='samples/', null=True, default=None)
    genotype_graph = models.URLField(null=True, default=None)
    date = models.DateTimeField(auto_now=True, verbose_name='Date')
    samples_group = models.IntegerField(default=1)  # 0 - one from many samples of subject,
    # 1 - only one sample to subject, and 2 - all the samples of the subject aggregated together.
    alleles = models.ManyToManyField(Alleles, through='Alleles_Samples', through_fields=('sample', 'allele'), related_name='+')
    def get_absolute_url(self):
        return reverse('database:sample-add')

    def get_novel_count(self):
        return self.alleles.filter(novel=True).count()

    def __str__(self):
        return str(self.name)


class Alleles_Samples(models.Model):
    allele = models.ForeignKey(Alleles, on_delete=models.DO_NOTHING)
    sample = models.ForeignKey(Samples, on_delete=models.DO_NOTHING)
    patient = models.ForeignKey(Patients, on_delete=models.DO_NOTHING)
    hap = models.CharField(max_length= 20)
    kdiff = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def __unicode__(self):
        return str(self.sample) + str(self.allele) + str(self.hap)

    def __str__(self):
        return str(self.sample) + str(self.allele) + str(self.hap)


class Alleles_patterns(models.Model):
    pattern = models.ForeignKey(Alleles, on_delete=models.DO_NOTHING, related_name="pattern")
    allele_in_p = models.ForeignKey(Alleles, on_delete=models.DO_NOTHING, related_name="allele_in_p")


class Genes_distribution(models.Model):
    patient = models.ForeignKey(Patients, on_delete=models.CASCADE)
    sample = models.ForeignKey(Samples, on_delete=models.CASCADE)
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE)
    frequency = models.DecimalField(max_digits=6, decimal_places=4, default=0)
    count_by_clones = models.BooleanField() # false means count by sequences


class AlleleConfidenceReport(models.Model):
    category = models.CharField(max_length=50)
    notes = models.CharField(max_length=400)
    allele = models.ForeignKey(Alleles, on_delete=models.CASCADE)


class HaplotypeEvidence(models.Model):
    hap_gene = models.CharField(max_length=50)
    sample = models.ForeignKey(Samples, on_delete=models.CASCADE)
    allele = models.ForeignKey(Alleles, on_delete=models.CASCADE)
    counts = models.CharField(max_length=200, default='')



#########################################################################################
#  For the future when we would like to expand the database for genomic samples too
#
#########################################################################################
#
# class Genomic_Samples(models.Model):
#     name = models.CharField(max_length=250, unique=True)
#     study = models.ForeignKey(Studies, on_delete=models.CASCADE)
#     patient = models.ForeignKey(Patients, on_delete=models.DO_NOTHING)
#     seq_protocol = models.ForeignKey(Seq_Protocols, on_delete=models.DO_NOTHING)
#     tissue_pro = models.ForeignKey(Tissue_Pro, on_delete=models.DO_NOTHING)
#     ig_group = models.CharField(max_length=20, choices=IG_GROUP_CHOICES)
#     haplotypeA = models.FileField(upload_to='genomic_samples/', null=True, default=None)
#     haplotypeB = models.FileField(upload_to='genomic_samples/', null=True, default=None)
#     genotype = models.FileField(upload_to='genomic_samples/', null=True, default=None)
#     genotype_graph = models.URLField(null = True, default = None)
#     reference = models.URLField(null=True, default=None)
#     date = models.DateField(auto_now=True, verbose_name='Date')
#     priority = models.IntegerField(default=1)
#
#     # def get_absolute_url(self):
#     #     return reverse('database:sample-add')
#
#     def __str__(self):
#         return str(self.name)
#
#
# class Alleles_Geno_Samples(models.Model):
#     allele = models.ForeignKey(Alleles, on_delete=models.DO_NOTHING)
#     sample = models.ForeignKey(Genomic_Samples, on_delete=models.DO_NOTHING)
#     patient = models.ForeignKey(Patients, on_delete=models.DO_NOTHING)
#     hap = models.CharField(max_length= 20, choices=TYPE_HAP)
#     kdiff = models.DecimalField(max_digits=6, decimal_places=2, default=0)
#
#     def __unicode__(self):
#         return str(self.sample) + str(self.allele) + str(self.hap)
#
#     def __str__(self):
#         return str(self.sample) + str(self.allele) + str(self.hap)
#
