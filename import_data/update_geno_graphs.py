#!/usr/bin/python
import sqlite3
import os
import sys, getopt


########################################################################################################################
#
# update the genotype graph url of the samples
#
########################################################################################################################
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def main(argv):
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
    c = conn.cursor()

    c.execute("SELECT name FROM database_samples")
    samples = c.fetchall()
    for sample in samples:
        geno_graph = "/Graph/genotypes/Genotype_html_" + sample[0] + ".html"
        c.execute("UPDATE database_samples SET genotype_graph = ? WHERE name = ?", (geno_graph, sample[0],))

    conn.commit()
    c.close

if __name__ == "__main__":
    main(sys.argv[1:])