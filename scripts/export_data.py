import xlwt
import os

from website.settings import SCRATCH_ROOT

METADATA_FOLDER = os.path.join(SCRATCH_ROOT, "tables", "metadata")

def export_sheet(sheet, arr):
    row=1
    for col in arr:
        i = 0
        for field in col:
            try:
                sheet.write(row, i, field)
            except:
                pass
            i+=1
        row+=1


def export_studies(book, studies):
    sheet = book.add_sheet("Studies")

    sheet.write(0,0, "Project")
    sheet.write(0,1, "Institute")
    sheet.write(0,2, "Researcher")
    sheet.write(0,3, "Number of Subjects")
    sheet.write(0,4, "Number of Samples")
    sheet.write(0,5, "Refernce")
    sheet.write(0,6, "Contact")
    sheet.write(0,7, "Accession id")

    export_sheet(sheet, studies)


def export_patients(book, patients):
    sheet = book.add_sheet("Subjects")

    sheet.write(0,0, "Name")
    sheet.write(0,1, "Sex")
    sheet.write(0,2, "Ethnic")
    sheet.write(0,3, "Health Status")
    sheet.write(0,4, "Age")
    sheet.write(0,5, "Cohort")

    export_sheet(sheet, patients)


def export_seq_protocols(book, seq_protocols):
    sheet = book.add_sheet("Sequence Protocol")

    sheet.write(0,0, "Name")
    sheet.write(0,1, "Sequencing_platform")
    sheet.write(0,2, "Sequencing_length")
    sheet.write(0,3, "Umi")
    sheet.write(0,4, "Helix")
    sheet.write(0,5, "5 primers location")
    sheet.write(0,6, "3 primers location")

    export_sheet(sheet, seq_protocols)


def export_tissue_pro (book, tissue_pros):
    sheet = book.add_sheet("Tissue Processing")

    sheet.write(0,0, "Name")
    sheet.write(0,1, "Specie")
    sheet.write(0,2, "Tissue")
    sheet.write(0,3, "Cell Type")
    sheet.write(0,4, "Sub Cell Type")
    sheet.write(0,5, "Isotype")

    export_sheet(sheet, tissue_pros)


def export_geno_detections(book, geno_detects):
    sheet = book.add_sheet("Genotype Detections")

    sheet.write(0,0, "Pipeline name")
    sheet.write(0,1, "Repertoire/Germline")
    sheet.write(0,2, "Pre-processing")
    sheet.write(0,3, "Aligner tool")
    sheet.write(0,4, "version")
    sheet.write(0,5, "Alignment_reference")
    sheet.write(0,6, "genotyped tool")
    sheet.write(0,7, "version")
    sheet.write(0,8, "haplotype tool")
    sheet.write(0,9, "version")
    sheet.write(0,10, "single alignment (True\False)")

    export_sheet(sheet, geno_detects)


def export_samples(book,samples):
    sheet = book.add_sheet("Samples")

    sheet.write(0,0, "Name")
    sheet.write(0,1, "Sequence Protocol")
    sheet.write(0,2, "Tissue Processing")
    sheet.write(0,3, "Subject")
    sheet.write(0,4, "Chain(heavy/light)")
    sheet.write(0,5, "Reads")
    sheet.write(0,6, "Geno Detection")
    sheet.write(0,7, "Genotype file")
    sheet.write(0,8, "Genotype stats file")
    sheet.write(0,9, "Haplotype files")
    sheet.write(0,10, "Project")

    export_sheet(sheet, samples)


def output(filename, titles, samples):
    if not os.path.exists(METADATA_FOLDER):
        os.makedirs(METADATA_FOLDER)

    path = os.path.join(METADATA_FOLDER, filename)
    metafile = open(path, "w", encoding='utf-8', errors='replace')
    metafile.write("\t".join(titles) + "\n")
    for samp in samples:
        metafile.write("\t".join(map(str, samp)) + "\n")

    metafile.close()
    # book = xlwt.Workbook()
    #
    # export_studies(book, studies)
    # export_samples(book, samples)
    # export_patients(book, patients)
    # export_seq_protocols(book, seq_protocols)
    # export_tissue_pro(book, tissue_pros)
    # export_geno_detections(book, geno_detects)
    #
    # book.save(path)
    return path

