#!/usr/bin/python
import sqlite3
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")
c = conn.cursor()

c.execute("DELETE FROM database_genes_distribution")
c.execute("DELETE FROM database_alleles_samples")
c.execute("DELETE FROM database_samples_haplotype")
c.execute("DELETE FROM database_alleles_patterns")
conn.commit()

c.execute("DELETE FROM database_samples")
c.execute("DELETE FROM database_haplotypes_files")
conn.commit()

c.execute("DELETE FROM database_seq_protocols")
c.execute("DELETE FROM database_tissue_pro")
c.execute("DELETE FROM database_geno_detection")
conn.commit()

c.execute("DELETE FROM database_patients")
c.execute("DELETE FROM database_alleles")
conn.commit()

c.execute("DELETE FROM database_gene")
c.execute("DELETE FROM database_studies")
conn.commit()

print("All the data in the database has been deleted!")