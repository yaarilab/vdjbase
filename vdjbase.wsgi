import os
import os.path
import sys
import site
from shutil import copyfile, rmtree

sys.path = ['/l_mnt/as14/d/website/www.vdjbase.org/vdjbase', '/l_mnt/as14/d/website/www.vdjbase.org/venv/VDJBASE/lib/python3.6/site-packages'] + sys.path

activate_this = '/l_mnt/as14/d/website/www.vdjbase.org/venv/VDJBASE/bin/activate_this.py'
exec(open(activate_this).read(), dict(__file__=activate_this))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
os.chdir('/l_mnt/as14/d/website/www.vdjbase.org/vdjbase')


from django.core.wsgi import get_wsgi_application

#from scripts import graphs


# If there is a file called 'update_db in the project directory and it is more recent than the sqlite database in scratch, copy the database to its writable location

if(os.path.isfile('update_db') and (not os.path.isfile('../scratch/db.sqlite3') or (os.path.getmtime('update_db') > os.path.getmtime('../scratch/db.sqlite3')))):
    os.system('rm -rf ../scratch/*')

    # Create the subdirectories in 'static'

    staticpath = '/l_mnt/as14/d/website/www.vdjbase.org/scratch/'
    subdirs = [
        'graphs',
        'graphs/alleles_appearance_graph',
        'graphs/freq_distributions',
        'graphs/genesXalleles',
        'graphs/heterozygous_graph',
        'graphs/merge_genotype',
        'graphs/genotypes',
        'tables',
        'tables/merge_genotypes',
    ]

    for sub in subdirs:
        if(not os.path.isdir(staticpath + sub)):
            os.mkdir(staticpath+sub)

    # If there is a file called 'genotypes.zip' in the project directory, unzip it into scratch

    if(os.path.isfile('genotypes.zip') and os.path.isdir('../scratch/graphs/genotypes')):
        os.system('unzip genotypes.zip -d ../scratch/graphs')

    copyfile('db.sqlite3', '../scratch/db.sqlite3')

# Add our local copy of R libraries

os.environ['R_LIBS'] = '/l_mnt/as14/d/website/www.vdjbase.org/R_library'
os.chdir('/l_mnt/as14/d/website/www.vdjbase.org/scratch')

application = get_wsgi_application()
