#!/usr/bin/python
import xlrd, sqlite3
import os, datetime
import sys, getopt, math
from shutil import copyfile


sys.path.append('.')
from sample_alleles import sample_genotype, sample_haplotype

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def maping_names(inputfile):
    """
    Renaming function, create dictionary of names according to the New Names sheet.
    :param inputfile: the path of the
    :return: dictionary of names, where the old names are the keys and the new names are the values
    """
    book = xlrd.open_workbook(inputfile)
    sheet = book.sheet_by_name("New Names")

    names_map = dict()
    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for r in range(1, sheet.nrows):
        original_name = sheet.cell(r, 0).value.replace(" ", "")
        new_name = sheet.cell(r, 1).value.replace(" ", "")
        names_map[original_name] = new_name

    return names_map


def import_study(sheet, conn):
    """
    Insert the study into the db
    :param sheet: the sheet in the metadata file
    :param conn: the connection to sqlite3
    :return: the study ID and name
    """
    c = conn.cursor()
    insert_study_query = "INSERT INTO database_studies (name, institute, researcher, num_subjects, num_samples, " \
                         "reference, contact, accession_id, accession_reference) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"

    row = 1 # 2nd row

    # extract the values from the sheet
    name = sheet.cell(row, 0).value.replace(" ","")
    institute = sheet.cell(row, 1).value.strip()
    researcher = sheet.cell(row, 2).value.strip()
    num_subjects = sheet.cell(row, 3).value
    num_samples = sheet.cell(row, 4).value
    reference = sheet.cell(row, 5).value.strip()
    contact = sheet.cell(row, 6).value.strip()
    accession_id = sheet.cell(row, 7).value.strip()
    accession_reference = sheet.cell(row, 8).value.strip()

    # check if study exists
    c.execute("SELECT id FROM database_studies WHERE name = ?", (name, ))
    study = c.fetchall()
    if study != []:
        return study[0][0], name

    # excute the query
    values = (name, institute, researcher, num_subjects, num_samples,reference, contact, accession_id, accession_reference)
    c.execute(insert_study_query, values)
    conn.commit()
    return c.lastrowid, name


def import_patients(study_id, sheet, conn):
    """
    import the the patients from the table into the patients table of the database.
    :param study_id: the study's ID
    :param sheet: the sheet of the subjects in the metadata file.
    :param conn: the connection object to the sql db.
    :param names_map: the names dictionary for renaming in case of new names.
    """
    c = conn.cursor()

    # Create the INSERT INTO sql query
    insert_subject_query = "INSERT INTO database_patients (name, name_in_paper, sex, ethnic, country, study_id, status, " \
                           "age, cohort) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"

    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for row in range(1, sheet.nrows):
        name = sheet.cell(row, 0).value.replace(" ","")
        name_in_paper = sheet.cell(row, 1).value.strip()
        sex = sheet.cell(row, 2).value.strip()
        ethnic = sheet.cell(row, 3).value.strip()
        country = sheet.cell(row, 4).value.strip()
        status = sheet.cell(row, 5).value.strip()
        age = sheet.cell(row, 6).value
        cohort = sheet.cell(row, 7).value.strip()

        if sex:
            if "f" in sex.lower():
                sex = "F"
            elif "m" in sex.lower():
                sex = "M"

        # Assign values from each row
        values = (name, name_in_paper, sex, ethnic, country, study_id, status, age, cohort)

        try:
            c.execute(insert_subject_query, values)
            print(name + " uploaded!")
        except:
            print(name, " patient exist")

    c.close()
    conn.commit()


def import_seq_protocols(sheet, conn):
    """
    import the information about the sequence protocols into the seq_protocol table in th DB.
    :param sheet: the sheet of the sequence protocol in the metadata file.
    :param conn: the connection object to the sql db.
    """
    c = conn.cursor()
    # Create the INSERT INTO sql query
    insert_seq_protocol_query = "INSERT INTO database_seq_protocols (name, umi, helix, primers_3_location, " \
                                "primers_5_location, sequencing_length, sequencing_platform) VALUES (?, ?, ?, ?, ?, ?, ?)"

    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for row in range(1, sheet.nrows):
        name = sheet.cell(row, 0).value.replace(" ","")
        c.execute("SELECT id FROM database_seq_protocols WHERE name = ?", (name,))
        id = c.fetchall()

        if id == [] or id[0] == []:
            sequencing_platform = sheet.cell(row, 1).value.strip()
            sequencing_length = sheet.cell(row, 2).value.strip()
            umi = sheet.cell(row, 3).value
            helix = sheet.cell(row, 4).value.lower()
            ps_5_location = sheet.cell(row, 5).value.strip()
            ps_3_location = sheet.cell(row, 6).value.strip()

            if (not umi) | (isinstance(umi, float)):
                umi = False
            elif umi.lower() in "true":
                umi = True
            elif umi.lower() in "false":
                umi = False
            elif umi == "":
                umi = False

            # Assign values from each row
            values = (name, umi, helix, ps_3_location, ps_5_location, sequencing_length, sequencing_platform)

            try:
                # Execute sql Query
                c.execute(insert_seq_protocol_query, values)
                print(name + " uploaded!")
            except:
                print("error in inserting sequence protocol: ", values)
                sys.exit()


    c.close()
    conn.commit()


def import_tissue_pro(sheet, conn):
    """
    import the information about the tissue protocols into the tissue_protocol table in th DB.
    :param sheet: the sheet of the tissue processing in metadata file.
    :param conn: the connection object to the sql db.
    """
    c = conn.cursor()

    # Create the INSERT INTO sql query
    insert_tissue_query = """INSERT INTO database_tissue_pro (name, species, tissue, cell_type, sub_cell_type, isotype)
    VALUES (?, ?, ?, ?, ?, ?)"""

    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for r in range(1, sheet.nrows):

        name = sheet.cell(r, 0).value.replace(" ","")
        c.execute("SELECT id FROM database_tissue_pro WHERE name = ?", (name,))
        id = c.fetchall()
        if id == [] or id[0] == []:
            species = sheet.cell(r, 1).value.strip()
            tissue = sheet.cell(r, 2).value.strip()
            cell_type = sheet.cell(r, 3).value.strip()
            sub_cell_type = sheet.cell(r, 4).value.strip()
            isotype = sheet.cell(r, 5).value.strip()

            # Assign values from each row
            values = (name, species, tissue, cell_type, sub_cell_type, isotype)

            try:
                # Execute sql Query
                c.execute(insert_tissue_query, values)
                print(name + " uploaded!")
            except:
                print("error in inserting tissue processing: ", name)
                sys.exit()

    conn.commit()
    c.close()


def import_geno_detection(sheet, conn):
    """
    import the information about the genotype inferring into the geno_detection table in th DB.
    :param sheet: the sheet of the geno_detection in metadata file.
    :param conn: the connection object to the sql db.
    """
    c = conn.cursor()

    # Create the INSERT INTO sql query
    query = """INSERT INTO database_geno_detection (name, detection, prepro_tool, aligner_tool, aligner_ver,
     aligner_reference, geno_tool, geno_ver, haplotype_tool, haplotype_ver, single_assignment) VALUES 
     (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for r in range(1, sheet.nrows):
        name = sheet.cell(r, 0).value.replace(" ","")
        detection = sheet.cell(r, 1).value.strip()
        prepro_tool = sheet.cell(r, 2).value.strip()
        aligner_tool = sheet.cell(r, 3).value.strip()
        aligner_ver = sheet.cell(r, 4).value.strip()
        aligner_reference = sheet.cell(r, 5).value.strip()
        geno_tool = sheet.cell(r, 6).value.strip()
        geno_ver = sheet.cell(r, 7).value.strip()
        haplotype_tool = sheet.cell(r, 8).value.strip()
        haplotype_ver = sheet.cell(r, 9).value.strip()
        single_assignment = sheet.cell(r, 10).value.strip()

        if not single_assignment:
            single_assignment = False
        elif single_assignment == True:
            single_assignment = True
        else:
            single_assignment = single_assignment.lower()
            if single_assignment in "true":
                single_assignment = True
            else:
                single_assignment = False

        # Assign values from each row
        values = (name, detection, prepro_tool, aligner_tool, aligner_ver, aligner_reference,
                  geno_tool, geno_ver, haplotype_tool, haplotype_ver, single_assignment)

        c.execute("SELECT id, detection FROM database_geno_detection WHERE name  = ?", (name,))
        geno_det = c.fetchall()
        if not geno_det:
            try:
                # Execute sql Query
                c.execute(query, values)
                print(name + " uploaded!")
            except:
                print("error in inserting geno: ", name)
                sys.exit()
        else:
            print(name + " exists")

    conn.commit()
    c.close()


def import_samples(path, study_name, study_id, sheet, conn):
    """
    import the the samples from the table into the Samples table of the database.
    :param path: the full path to the files
    :param study_name: the study's name
    :param study_id: the study's ID
    :param sheet: the sheet of the samples metadata file.
    :param conn: the connection object to the sql db.
    """
    c = conn.cursor()

    ptarget = BASE_DIR + '/uploads/samples/' + study_name + "/"
    os.system("mkdir " + ptarget)

    # Create the INSERT INTO sql query
    insert_sample_query = """INSERT INTO database_samples (name, seq_protocol_id, tissue_pro_id, patient_id, chain,
     row_reads, geno_detection_id, genotype, genotype_stats, study_id, date, samples_group) 
     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    insert_haplotype_file_query = "INSERT INTO database_haplotypes_files (by_gene, allele_col1, allele_col2, file) " \
                                  "VALUES (?, ?, ?, ?)"

    insert_samp_haplo_query = "INSERT INTO database_samples_haplotype (samples_id, haplotypes_files_id) VALUES (?, ?)"

    # Create a For loop to iterate through each row in the XLS file, starting at row 1
    for r in range(1, sheet.nrows):
        name = sheet.cell(r, 0).value.replace(" ","")
        seq_protocol = sheet.cell(r, 1).value.replace(" ","")
        tissue_pro = sheet.cell(r, 2).value.replace(" ","")
        patient = sheet.cell(r, 3).value.replace(" ","")
        chain = sheet.cell(r, 4).value.replace(" ","")
        reads = int(sheet.cell(r, 5).value)
        geno_det = sheet.cell(r, 6).value.replace(" ","")
        genotype_file = sheet.cell(r, 7).value.replace(" ","")
        genotype_stat_file = sheet.cell(r, 8).value.replace(" ","")
        haplotype_path = sheet.cell(r, 9).value.replace(" ","")
        haplotype_genes = sheet.cell(r, 10).value.replace(" ","")
        samples_group = int(sheet.cell(r, 11).value)

        p = ptarget + patient + "/"
        p = p.replace(" ","", p.count(" "))
        new_loc = "samples/" + study_name + "/"+ patient + "/"
        os.system("mkdir " + p)

        c.execute("SELECT id FROM database_patients WHERE name  = ?", (patient,))
        pid = c.fetchall()[0][0]

        # getting the sequencing protocol ID
        c.execute("SELECT id FROM database_seq_protocols WHERE name  = ?", (seq_protocol,))
        seq_id = c.fetchall()[0][0]

        # getting the tissue processing ID
        c.execute("SELECT id FROM database_tissue_pro WHERE name  = ?", (tissue_pro,))
        tissue_id = c.fetchall()
        tissue_id = tissue_id[0][0]

        # getting the genotype detection ID
        c.execute("SELECT id FROM database_geno_detection WHERE name  = ?", (geno_det,))
        geno_det = c.fetchall()[0]
        geno_det_id = geno_det[0]

        # checking existing of genotype file
        if genotype_file == "":
            print("missing genotype file in " + name)
            sys.exit()

        src = path + genotype_file
        src = src.replace(" ", "")
        dst = p + genotype_file.split("/")[-1]
        dst = dst.replace(" ", "")

        if not os.path.isfile(src):
            continue

        copyfile(src, dst)
        genotype_file = new_loc + genotype_file.split("/")[-1]

        haplotypes_ids = []
        if haplotype_genes.upper() != "NA":
            for haplo_segment in haplotype_genes.split(","):
                gene = "-".join(haplo_segment.split("-")[:-1])
                alleles = haplo_segment.split("-")[-1]
                allele1 = gene + "_" + alleles.split("_")[0]
                allele2 = gene + "_" + alleles.split("_")[1]
                src = path + haplotype_path + haplo_segment + "_haplotype.tab"

                src = src.replace(" ", "")
                # replace the name of the original name of the subject
                haplotype_file = src.split("/")[-1]
                dst = p + haplotype_file.split("/")[-1]
                dst = dst.replace(" ", "")

                copyfile(src, dst)
                haplotype_file = new_loc + haplotype_file.split("/")[-1]

                haplo_values = (haplo_segment, allele1, allele2, haplotype_file)
                c.execute(insert_haplotype_file_query, haplo_values)
                conn.commit()

                haplotypes_ids.append(c.lastrowid)


        if genotype_stat_file == "":
            print("missing genotype stats file in " + name)
            # sys.exit()

        src = path + genotype_stat_file
        if os.path.isfile(src):
            src = src.replace(" ","")
            dst = p + genotype_stat_file.split("/")[-1]
            dst = dst.replace(" ", "")

            copyfile(src, dst)
            genotype_stat_file = new_loc + genotype_stat_file.split("/")[-1]

        else:
            genotype_stat_file = None


        print("Uploading sample " + name)
        # Assign values from each row
        values = (name, seq_id, tissue_id, pid, chain, reads, geno_det_id,
                  genotype_file, genotype_stat_file, study_id, datetime.datetime.today(), samples_group)

        c.execute("SELECT id FROM database_samples WHERE name = ?", (name,))
        sample_id = c.fetchall()
        if sample_id != []:
            sample_id = sample_id[0][0]

        else:
            c.execute(insert_sample_query, values)
            sample_id = c.lastrowid

        conn.commit()
        sample_genotype(genotype_file, sample_id)
        conn.commit()

        if haplotypes_ids != []:
            for haplo_id in haplotypes_ids:
                samp_hap_vals = (sample_id, haplo_id)
                c.execute(insert_samp_haplo_query, samp_hap_vals)

                # c.execute("SELECT file, allele_col1, allele_col2 FROM database_haplotypes_files WHERE id = ?", (haplo_id,))
                # temp = c.fetchall()[0]
                # haplotype = temp[0]
                # allele1 = temp[1]
                # allele2 = temp[2]
                # sample_haplotype(haplotype, sample_id, [allele1, allele2], conn)

        conn.commit()

    conn.commit()
    c.close()