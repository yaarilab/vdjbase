#!/usr/bin/python
import xlrd, sqlite3
import os, datetime
import sys, getopt
from shutil import copyfile
sys.path.append('.')
import upload_metadata_table as up
import update_the_analyzed_data as ua
import calculate_patterns as cp

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def main(argv):
    # getting the metadata filename
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        print('import_data.py -i <inputfile>')
        sys.exit(2)
    inputfile = 'missing'
    for opt, arg in opts:
        if opt == '-h':
            print('import_data.py -i <inputfile> \n\n\n'
                  '-h 13 --help \n'
                  'information about the input parameters \n\n'
                  '-i --inputfile\n'
                  'the input file itself (including the path)\n\n')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
    if inputfile == 'missing':
        print('input file is missing')
        sys.exit()

    # the relative path for the files
    path = "/".join(inputfile.split("/")[:-1]) + "/"

    # Establish a sqlite3 connection
    conn = sqlite3.connect(BASE_DIR + "/db.sqlite3")

    # First it's needed to upload the study and get its id.
    book = xlrd.open_workbook(inputfile)
    # upload to study table
    sheet = book.sheet_by_name("Studies")
    study_id, study_name = up.import_study(sheet, conn)
    # upload to Patients table
    sheet = book.sheet_by_name("Subjects")
    up.import_patients(study_id, sheet, conn)
    # upload the sequencing protocol
    sheet = book.sheet_by_name("Sequence Protocol")
    up.import_seq_protocols(sheet, conn)
    # upload to tissue processing table
    sheet = book.sheet_by_name("Tissue Processing")
    up.import_tissue_pro(sheet, conn)
    # upload to genotype detection table
    sheet = book.sheet_by_name("Genotype Detections")
    up.import_geno_detection(sheet, conn)
    # upload the samples
    sheet = book.sheet_by_name("Samples")
    up.import_samples(path, study_name, study_id, sheet, conn)

    # Commit the transaction
    conn.commit()
    # Close the database connection
    conn.close()

    ua.update_alleles_appearance()
    print("Done matching alleles to samples.")
    ua.calculate_genes_frequencies()
    print("Done checking the gene usage of the samples")
    cp.check_patterns()
    print("Done checking for new possible patterns")
    # Print results
    print("")
    print("All Done! Bye, for now.")
    print("")

if __name__ == "__main__":
    main(sys.argv[1:])